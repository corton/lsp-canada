<?php
class ControllerShippingCanadaPost extends Controller {
	private $error = array();
	private $name = '';

	public function index() {

		/* START ERRORS */
		$errors = array(
			'warning',
			'mid',
			'postcode',
			'service'
		);
		/* END ERRORS */



		/* START COMMON STUFF */
		$this->name = basename(__FILE__, '.php');

		if (!isset($this->session->data['token'])) { $this->session->data['token'] = 0; }
		$this->data['token'] = $this->session->data['token'];
		$this->data = array_merge($this->data, $this->load->language('shipping/' . $this->name));

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate($errors))) {
			foreach ($this->request->post as $key => $value) {
				if (is_array($value)) { $this->request->post[$key] = implode(',', $value); }
			}

			$this->load->model('setting/setting');

			$this->model_setting_setting->editSetting($this->name, $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect((((HTTPS_SERVER) ? HTTPS_SERVER : HTTP_SERVER) . 'index.php?token=' . $this->session->data['token'] . '&route=extension/shipping'));
		}

		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'href'      => (((HTTPS_SERVER) ? HTTPS_SERVER : HTTP_SERVER) . 'index.php?token=' . $this->session->data['token'] . '&route=common/home'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

   		$this->data['breadcrumbs'][] = array(
       		'href'      => (((HTTPS_SERVER) ? HTTPS_SERVER : HTTP_SERVER) . 'index.php?token=' . $this->session->data['token'] . '&route=extension/shipping'),
       		'text'      => $this->language->get('text_shipping'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'href'      => (((HTTPS_SERVER) ? HTTPS_SERVER : HTTP_SERVER) . 'index.php?token=' . $this->session->data['token'] . '&route=shipping/' . $this->name),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);

		$this->data['action'] = (((HTTPS_SERVER) ? HTTPS_SERVER : HTTP_SERVER) . 'index.php?token=' . $this->session->data['token'] . '&route=shipping/' . $this->name);

		$this->data['cancel'] = (((HTTPS_SERVER) ? HTTPS_SERVER : HTTP_SERVER) . 'index.php?token=' . $this->session->data['token'] . '&route=extension/shipping');

		$this->id       = 'content';
		$this->template = 'shipping/' . $this->name . '.tpl';

		/* 14x backwards compatibility */
		if (method_exists($this->document, 'addBreadcrumb')) { //1.4.x
			$this->document->breadcrumbs = $this->data['breadcrumbs'];
			unset($this->data['breadcrumbs']);
		}//

		$this->children = array(
            'common/header',
            'common/footer'
        );

        foreach ($errors as $error) {
			if (isset($this->error[$error])) {
				$this->data['error_' . $error] = $this->error[$error];
			} else {
				$this->data['error_' . $error] = '';
			}
		}
		/* END COMMON STUFF */




		/* START FIELDS */
		$this->data['extension_class'] = 'shipping';
		$this->data['tab_class'] = 'htabs';

		$geo_zones = array();

		$this->load->model('localisation/geo_zone');

		$geo_zones[0] = $this->language->get('text_all_zones');
		foreach ($this->model_localisation_geo_zone->getGeoZones() as $geozone) {
			$geo_zones[$geozone['geo_zone_id']] = $geozone['name'];
		}

		$order_statuses = array();

		$this->load->model('localisation/order_status');

		foreach ($this->model_localisation_order_status->getOrderStatuses() as $order_status) {
			$order_statuses[$order_status['order_status_id']] = $order_status['name'];
		}

		$customer_groups = array();

		$this->load->model('sale/customer_group');

		foreach ($this->model_sale_customer_group->getCustomerGroups() as $customer_group) {
			$customer_groups[$customer_group['customer_group_id']] = $customer_group['name'];
		}

		$tax_classes = array();

		$this->load->model('localisation/tax_class');
		
		$tax_classes[0] = $this->language->get('text_none');
		foreach ($this->model_localisation_tax_class->getTaxClasses() as $tax_class) {
			$tax_classes[$tax_class['tax_class_id']] = $tax_class['title'];
		}

		$this->data['tabs'] = array();

		$this->data['tabs'][] = array(
			'id'		=> 'tab_general',
			'title'		=> $this->language->get('tab_general')
		);
		
		$this->data['tabs'][] = array(
			'id'		=> 'tab_debug',
			'title'		=> $this->language->get('tab_debug')
		);

		$this->data['fields'] = array();

		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_status'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_status',
			'value' 		=> (isset($this->request->post[$this->name . '_status'])) ? $this->request->post[$this->name . '_status'] : $this->config->get($this->name . '_status'),
			'required' 		=> false,
			'options'		=> array(
				'0' => $this->language->get('text_disabled'),
				'1' => $this->language->get('text_enabled')
			)
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_mid'),
			'type'			=> 'text',
			'size'			=> '40',
			'name' 			=> $this->name . '_mid',
			'value' 		=> (isset($this->request->post[$this->name . '_mid'])) ? $this->request->post[$this->name . '_mid'] : $this->config->get($this->name . '_mid'),
			'required' 		=> true,
			'help'			=> $this->language->get('help_mid'),
			'error'			=> (isset($this->error['mid'])) ? $this->error['mid'] : ''
		);

		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_postcode'),
			'type'			=> 'text',
			'size'			=> '20',
			'name' 			=> $this->name . '_postcode',
			'value' 		=> (isset($this->request->post[$this->name . '_postcode'])) ? $this->request->post[$this->name . '_postcode'] : $this->config->get($this->name . '_postcode'),
			'required' 		=> true,
			'error'			=> (isset($this->error['postcode'])) ? $this->error['postcode'] : ''
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_lettermail'),
			'type'			=> 'text',
			'size'			=> '80',
			'name' 			=> $this->name . '_lettermail',
			'value' 		=> (isset($this->request->post[$this->name . '_lettermail'])) ? $this->request->post[$this->name . '_lettermail'] : $this->config->get($this->name . '_lettermail'),
			'required' 		=> false,
			'help' 			=> $this->language->get('help_lettermail'),
			'error'			=> (isset($this->error['lettermail'])) ? $this->error['lettermail'] : ''
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_lettermail_us'),
			'type'			=> 'text',
			'size'			=> '80',
			'name' 			=> $this->name . '_lettermail_us',
			'value' 		=> (isset($this->request->post[$this->name . '_lettermail_us'])) ? $this->request->post[$this->name . '_lettermail_us'] : $this->config->get($this->name . '_lettermail_us'),
			'required' 		=> false,
			'help' 			=> $this->language->get('help_lettermail_us'),
		);

		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_lettermail_int'),
			'type'			=> 'text',
			'size'			=> '80',
			'name' 			=> $this->name . '_lettermail_int',
			'value' 		=> (isset($this->request->post[$this->name . '_lettermail_int'])) ? $this->request->post[$this->name . '_lettermail_int'] : $this->config->get($this->name . '_lettermail_int'),
			'required' 		=> false,
			'help' 			=> $this->language->get('help_lettermail_int'),
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_service'),
			'type'			=> 'select',
			'size'			=> '12',
			'multiple'		=> true,
			'name' 			=> $this->name . '_service[]',
			'value' 		=> explode(',', (isset($this->request->post[$this->name . '_service'])) ? $this->request->post[$this->name . '_service'] : $this->config->get($this->name . '_service')),
			'required' 		=> true,
			'options'		=> array(
				'1010' => $this->language->get('text_1010'),
				'1020' => $this->language->get('text_1020'),
				'1030' => $this->language->get('text_1030'),
				'1040' => $this->language->get('text_1040'),
				'2000' => $this->language->get('text_2000'),
				'2005' => $this->language->get('text_2005'),
				'2015' => $this->language->get('text_2015'),
				'2020' => $this->language->get('text_2020'),
				'2030' => $this->language->get('text_2030'),
				'2040' => $this->language->get('text_2040'),
				'3000' => $this->language->get('text_3000'),
				'3005' => $this->language->get('text_3005'),
				'3010' => $this->language->get('text_3010'),
				'3015' => $this->language->get('text_3015'),
				'3025' => $this->language->get('text_3025'),
				'3040' => $this->language->get('text_3040')
			),
			'error'			=> (isset($this->error['service'])) ? $this->error['service'] : ''
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_package_info'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_package_info',
			'value' 		=> (isset($this->request->post[$this->name . '_package_info'])) ? $this->request->post[$this->name . '_package_info'] : $this->config->get($this->name . '_package_info'),
			'required' 		=> false,
			'options'		=> array(
				'0' => $this->language->get('text_no'),
				'1' => $this->language->get('text_yes')
			)
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_individual'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_individual',
			'value' 		=> (isset($this->request->post[$this->name . '_individual'])) ? $this->request->post[$this->name . '_individual'] : $this->config->get($this->name . '_individual'),
			'required' 		=> false,
			'options'		=> array(
				'0' => $this->language->get('text_no'),
				'1' => $this->language->get('text_yes')
			)
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_rts'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_rts',
			'value' 		=> (isset($this->request->post[$this->name . '_rts'])) ? $this->request->post[$this->name . '_rts'] : $this->config->get($this->name . '_rts'),
			'required' 		=> false,
			'options'		=> array(
				'0' => $this->language->get('text_no'),
				'1' => $this->language->get('text_yes')
			)
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_length'),
			'type'			=> 'text',
			'size'			=> '4',
			'name' 			=> $this->name . '_length',
			'value' 		=> (isset($this->request->post[$this->name . '_length'])) ? $this->request->post[$this->name . '_length'] : $this->config->get($this->name . '_length'),
			'required' 		=> false,
			'error'			=> (isset($this->error['length'])) ? $this->error['length'] : '',
			'help'			=> ($this->language->get('help_length')) ? $this->language->get('help_length') : '',
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_width'),
			'type'			=> 'text',
			'size'			=> '4',
			'name' 			=> $this->name . '_width',
			'value' 		=> (isset($this->request->post[$this->name . '_width'])) ? $this->request->post[$this->name . '_width'] : $this->config->get($this->name . '_width'),
			'required' 		=> false,
			'error'			=> (isset($this->error['width'])) ? $this->error['width'] : '',
			'help'			=> ($this->language->get('help_width')) ? $this->language->get('help_width') : '',
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_height'),
			'type'			=> 'text',
			'size'			=> '4',
			'name' 			=> $this->name . '_height',
			'value' 		=> (isset($this->request->post[$this->name . '_height'])) ? $this->request->post[$this->name . '_height'] : $this->config->get($this->name . '_height'),
			'required' 		=> false,
			'error'			=> (isset($this->error['height'])) ? $this->error['height'] : '',
			'help'			=> ($this->language->get('help_height')) ? $this->language->get('help_height') : '',
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_extra_cost'),
			'type'			=> 'text',
			'size'			=> '60',
			'name' 			=> $this->name . '_extra_cost',
			'value' 		=> (isset($this->request->post[$this->name . '_extra_cost'])) ? $this->request->post[$this->name . '_extra_cost'] : $this->config->get($this->name . '_extra_cost'),
			'required' 		=> false,
			'error'			=> (isset($this->error['extra_cost'])) ? $this->error['extra_cost'] : ''
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_display_weight'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_display_weight',
			'value' 		=> (isset($this->request->post[$this->name . '_display_weight'])) ? $this->request->post[$this->name . '_display_weight'] : $this->config->get($this->name . '_display_weight'),
			'required' 		=> false,
			'options'		=> array(
				'0' => $this->language->get('text_no'),
				'1' => $this->language->get('text_yes')
			)
		);
		
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_display_date'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_display_date',
			'value' 		=> (isset($this->request->post[$this->name . '_display_date'])) ? $this->request->post[$this->name . '_display_date'] : $this->config->get($this->name . '_display_date'),
			'required' 		=> false,
			'options'		=> array(
				'0' => $this->language->get('text_no'),
				'1' => $this->language->get('text_yes')
			)
		);

		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_debug'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_debug',
			'value' 		=> (isset($this->request->post[$this->name . '_debug'])) ? $this->request->post[$this->name . '_debug'] : $this->config->get($this->name . '_debug'),
			'required' 		=> false,
			'options'		=> array(
				'0' => $this->language->get('text_disabled'),
				'1' => $this->language->get('text_enabled')
			)
		);
/*
		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_customer_group'),
			'type'			=> 'select',
			'multiple'		=> true,
			'name' 			=> $this->name . '_customer_group[]',
			'value' 		=> (isset($this->request->post[$this->name . '_customer_group'])) ? $this->request->post[$this->name . '_customer_group'] : $this->config->get($this->name . '_customer_group'),
			'required' 		=> false,
			'options'		=> $customer_groups
		);
*/

		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_tax'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_tax_class_id',
			'value' 		=> (isset($this->request->post[$this->name . '_tax_class_id'])) ? $this->request->post[$this->name . '_tax_class_id'] : $this->config->get($this->name . '_tax_class_id'),
			'required' 		=> false,
			'options'		=> $tax_classes
		);

		$this->data['fields'][] = array(
			'entry' 		=> $this->language->get('entry_geo_zone'),
			'type'			=> 'select',
			'name' 			=> $this->name . '_geo_zone_id',
			'value' 		=> (isset($this->request->post[$this->name . '_geo_zone_id'])) ? $this->request->post[$this->name . '_geo_zone_id'] : $this->config->get($this->name . '_geo_zone_id'),
			'required' 		=> false,
			'options'		=> $geo_zones
		);

		$this->data['fields'][] = array(
			'entry'			=> $this->language->get('entry_sort_order'),
			'type'			=> 'text',
			'name'			=> $this->name . '_sort_order',
			'value'			=> (isset($this->request->post[$this->name . '_sort_order'])) ? $this->request->post[$this->name . '_sort_order'] : $this->config->get($this->name . '_sort_order'),
			'required'		=> false,
		);
		
		$this->data['fields'][] = array(
			'tab'			=> 'tab_debug',
			'entry'			=> '',
			'type'			=> 'textarea',
			'cols'			=> '160',
			'rows'			=> '100',
			'name'			=> '',
			'value'			=> (file_exists(DIR_LOGS . $this->name . '_debug.txt')) ? file_get_contents(DIR_LOGS . $this->name . '_debug.txt') : ''
		);
		/* END FIELDS */

        $this->response->setOutput($this->render(TRUE));
	}

	private function validate($errors = array()) {
		if (!$this->user->hasPermission('modify', 'shipping/' . $this->name)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($errors as $error) {
			if (isset($this->request->post[$this->name . '_' . $error]) && !$this->request->post[$this->name . '_' . $error]) {
				$this->error[$error] = $this->language->get('error_' . $error);
			}
			if ($error == 'service') {
				if (!isset($this->request->post[$this->name . '_service']) || !$this->request->post[$this->name . '_service']) {
					$this->error[$error] = $this->language->get('error_' . $error);
				}
			}
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>