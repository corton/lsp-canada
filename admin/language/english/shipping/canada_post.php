<?php
// Heading
$_['heading_title']    = 'Canada Post (Q)';

// Text
$_['text_shipping']    		= 'Shipping';
$_['text_success']     		= 'Success: You have modified Canada Post shipping!';

$_['text_1010']    	   		= 'CA - Regular';
$_['text_1020']    	   		= 'CA - Expedited';
$_['text_1030']    	   		= 'CA - Xpresspost';
$_['text_1040']    	   		= 'CA - Priority Courier';
$_['text_2000']    	   		= 'US - Tracked Packet';
$_['text_2005']    	   		= 'US - Small Packets Surface';
$_['text_2015']    	   		= 'US - Small Packets Air';
$_['text_2020']    	   		= 'US - Expedited US Business';
$_['text_2030']    	   		= 'US - Xpresspost USA';
$_['text_2040']    	   		= 'US - Priority Worldwide USA';
$_['text_3000']    	   		= 'INT - Tracked Packet';
$_['text_3005']    	   		= 'INT - Small Packets Surface';
$_['text_3010']    	   		= 'INT - Parcel Surface';
$_['text_3015']    	   		= 'INT - Small Packets Air';
$_['text_3025']    	   		= 'INT - XPressPost International';
$_['text_3040']    	   		= 'INT - Priority Worldwide INTL';

$_['tab_debug']		   		= 'Debug';

// Entry
$_['entry_postcode']   		= 'Post Code:';
$_['entry_mid']        		= 'Customer ID:';
$_['entry_extra_cost'] 		= 'Extra Cost:';
$_['entry_service']    		= 'Services:<br/><span class="help">Hold Ctrl+click to select multiple. Keep in mind that not all services will show for all shipments. It only shows the ones that Canada Post deems valid for your current cart details.</span>';
$_['entry_geo_zone']   		= 'Geo Zone:';
$_['entry_tax']        		= 'Tax Class:';
$_['entry_geo_zone']   		= 'Geo Zone:';
$_['entry_status']     		= 'Status:';
$_['entry_sort_order'] 		= 'Sort Order:';
$_['entry_lettermail'] 		= 'CA Lettermail Rates:<br/><span class="help">Leave blank to always return premium rates for cart weight less than 500g</span>';
$_['entry_lettermail_us'] 	= 'US Lettermail Rates:<br/><span class="help">Leave blank to always return premium rates for cart weight less than 500g</span>';
$_['entry_lettermail_int'] 	= 'Intl Lettermail Rates:<br/><span class="help">Leave blank to always return premium rates for cart weight less than 500g</span>';
$_['entry_debug']  	   		= 'Debug Mode:<br/><span class="help">(Save XML Request and Response to Debug log on the Debug Tab for analyzing)</span>';
$_['entry_individual'] 		= 'Individual Package Mode:<br/><span class="help">If Yes, each item in the cart will be rated as a separate package. If no, the cart will treat the entire cart as a single package.</span>';
$_['entry_package_info']	= 'Log Packaging Details to Comments:<br/><span class="help">If Yes, each item in the cart will be added to the order comments with details of which Box dimensions and packing order recommended by Canada Post based on the boxes added to your canada post account. This only works if "Ready To Ship" is disabled.</span>';
$_['entry_rts'] 	   		= 'Ready To Ship:<br /><span class="help">This sets individual packages into their own box and CP will not try to combine them. This is only used if you have Individual Packages set to Yes. When enabled, it will result in higher prices as each box is rated separately.</span>';
$_['entry_length'] 	   		= 'Avg Length (cm):';
$_['entry_width'] 	   		= 'Avg Width (cm):';
$_['entry_height'] 	   		= 'Avg Height (cm):';
$_['entry_display_weight'] 	= 'Display Weight Next to Rate:';
$_['entry_display_date'] 	= 'Display Delivery Date Next to Rate:';


// Help
$_['help_mid'] 		   		= '(Use CPC_DEMO_XML for testmode)';
$_['help_extra_cost']  		= 'Add extra handling cost to any rate';
$_['help_lettermail']  		= 'The Canada Post Rate server only offers premium rates. For packages less than 500g, you can set up Lettermail rates for your customers to select from. The format is weight:cost, weight:cost. All weights are in grams. (ex. 30:0.61, 50:1.05, 100:1.29, 200:2.10, 300:2.95, 400:3.40, 500:3.65). The weight implies the max weight for that rate. <a href="http://www.canadapost.ca/tools/pg/supportdocuments/lm_pricesheet-e.pdf" target="_blank">Canada Rates</a>';
$_['help_lettermail_us']  	= 'The Canada Post Rate server only offers premium rates. For packages less than 500g, you can set up Lettermail rates for your customers to select from. The format is weight:cost, weight:cost. All weights are in grams. (ex. 30:1.05, 50:1.29, 100:2.10, 200:3.70, 500:7.40). The weight implies the max weight for that rate. <a href="http://www.canadapost.ca/cpo/mc/business/productsservices/letterpost.jsf" target="_blank">US & INTL Rates</a>';
$_['help_lettermail_int']  	= 'The Canada Post Rate server only offers premium rates. For packages less than 500g, you can set up Lettermail rates for your customers to select from. The format is weight:cost, weight:cost. All weights are in grams. (ex. 30:1.80, 50:2.58, 100:4.20, 200:7.40, 500:14.80). The weight implies the max weight for that rate. <a href="http://www.canadapost.ca/cpo/mc/business/productsservices/letterpost.jsf" target="_blank">US & INTL Rates</a>';
$_['help_length'] 	   		= '<span class="help">If you set individual package mode to "NO", then the extension assumes you are using a predetermined box size to put all cart items into the box. Of course, depending on what is purchased, this may vary. So this is a rough estimated package size that will be sent as part of the rate request. For more accurate rats, use the individual package mode.</span>';
$_['help_width'] 	   		= '<span class="help">If you set individual package mode to "NO", then the extension assumes you are using a predetermined box size to put all cart items into the box. Of course, depending on what is purchased, this may vary. So this is a rough estimated package size that will be sent as part of the rate request. For more accurate rats, use the individual package mode.</span>';
$_['help_height'] 	   		= '<span class="help">If you set individual package mode to "NO", then the extension assumes you are using a predetermined box size to put all cart items into the box. Of course, depending on what is purchased, this may vary. So this is a rough estimated package size that will be sent as part of the rate request. For more accurate rats, use the individual package mode.</span>';


// Error
$_['error_permission'] 		= 'Warning: You do not have permission to modify Canada Post shipping!';
$_['error_postcode']   		= 'Post Code Required!';
$_['error_mid']  	   		= 'Customer ID Required!';
$_['error_service']    		= 'You must select at least one service!';
?>