<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="www.theme-opencart.com">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/start-online-store/stylesheet/color.css" media="screen" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/start-online-store/stylesheet/stylesheet.css" media="screen" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/start-online-store/stylesheet/responsive.css" media="screen" />
<!-- OPENCART FILE -->
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.total-storage.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen" />
<script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.cycle.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.jcarousel.min.js"></script>

<!-- OPENCART FILE -->
<script type="text/javascript" src="catalog/view/theme/start-online-store/javascript/jbmenu.js"></script>
<script type="text/javascript" src="catalog/view/theme/start-online-store/javascript/cloud-zoom.1.0.2.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/start-online-store/stylesheet/nivo-sos-skin.css" media="screen" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/start-online-store/stylesheet/nivo-slider.css" media="screen" />
<script type="text/javascript" src="catalog/view/theme/start-online-store/javascript/jquery.nivo.slider.js"></script>
<!--[if lt IE 9]>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>
</head>
<body>
<!-- START HTML -->
<div class="container" id="container">
  <!-- START HEADER -->
  <div class="header" id="header">
    <!-- START htop -->
    <div class="htop">
      <?php echo $cart; ?>
      <!-- START LEFT HEADER -->
      <div class="hleft">
        
        <!-- START LOGO -->
        <?php if ($logo) { ?>
        <div class="logo" id="logo">
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
        </div>
        <?php } ?>
        <!-- END LOGO -->
        <!-- START SEARCH -->
        <div class="search" id="search">
          <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" onClick="this.value = '';" onKeyDown="this.style.color = '#000000';" />
          <div class="button-search"></div>
        </div>
        <!-- END SEARCH -->
        
      </div>
      
      <!-- END LEFT HEADER -->
    </div>
    <!-- END htop -->
    <!-- START hbottom -->
    <div class="hbottom">
      <!-- START USER MENU -->
      <ul class="links">
      	<li><a class="home" href="<?php echo $home; ?>" title="<?php echo $text_home; ?>"><?php echo $text_home; ?></a></li>
      	<li><a href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a></li>
      	<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
      	<li><a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a></li>
      	<li><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
      </ul>
      <!-- END USER MENU -->
      <!-- START WELCOME -->
        <div class="welcome" id="welcome">
          <?php if (!$logged) { ?>
          <p class="login"><?php echo $text_welcome; ?></p>
          <?php } else { ?>
          <p class="user"><?php echo $text_logged; ?></p>
          <?php } ?>
        </div>
        <!-- END WELCOME -->
      <?php echo $currency; ?>
      <?php echo $language; ?>
    </div>
    <!-- END hbottom-->
    <!-- START hmenu -->
    <?php if ($categories) { ?>
    <div class="hmenu">
      <div class="menu" id="menu">
        <ul>
        <li><a href="index.php?route=common/home"><?php echo 'Home'; ?></a></li>
          <?php foreach ($categories as $category) { ?>
          <li><a <?php if ($category['children']) { ?>class="arrow"<?php } ?> href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
            <?php if ($category['children']) { ?>
            <div>
              <?php for ($i = 0; $i < count($category['children']);) { ?>
              <ul>
                <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
                <?php for (; $i < $j; $i++) { ?>
                <?php if (isset($category['children'][$i])) { ?>
                <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
                <?php } ?>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <?php } ?>
          </li>
          <?php } ?>
          <li><a href="index.php?route=information/information&information_id=8"><?php echo 'Website Service'; ?></a></li>
          <li><a href="http://ylprintstudios.com/" target="_blank"><?php echo 'Print Studio'; ?></a></li>
          <li><a href="index.php?route=information/contact"><?php echo 'Contact Us'; ?></a></li>
        </ul>
      </div>
    </div>
    <!-- END hmenu-->
    <!-- START jbmenu-->
    <div id="jbmenu" class="jbmenu">
      <!-- START MINI MENU -->
      <div class="jbtitle"><a>Menu</a></div>
      <div class="jbcontent">
      <ul>
        <?php foreach ($categories as $category) { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a><?php if ($category['children']) { ?><span class="jbchildren">&nbsp;</span><?php } ?>
          <?php if ($category['children']) { ?>
          <ul class="jbchildrenlist">
          <?php for ($i = 0; $i < count($category['children']);) { ?>
            <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
            <?php for (; $i < $j; $i++) { ?>
            <?php if (isset($category['children'][$i])) { ?>
            <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
            <?php } ?>
            <?php } ?>
          <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
      </div>
      <!-- END MINI MENU -->
    </div>
    <!-- END jbmenu -->
    <?php } ?>
  </div>
  <!-- END HEADER -->