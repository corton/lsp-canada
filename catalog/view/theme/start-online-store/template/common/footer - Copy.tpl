﻿</div>
<!-- END HTML -->
<!-- START FOOTER -->
<div id="footer" class="footer">
  <div class="top_footer">
    <div class="contact_us">
      <!-- SRART CONTACT US INFO -->
      <div class="title">CONTACT US</div>
      <ul>
        <li class="phone">
            <span class="icon"> </span>
            9999999999999
        </li>
        <li class="email">
            <span class="icon"> </span>
            <a href="mailto:XXXXX@YYYYY.com">XXXXX@YYYYY.com</a>
        </li>
        <li class="address">
            <span class="icon"> </span>
            ZZZZZZZZZZZZZZZZZ
        </li>
      </ul>
      <!-- END CONTACT US INFO -->
    </div><div class="about_us">
      <!-- SRART ABOUT US INFO -->
      <div class="title">ABOUT US</div>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ...</p>
      <div class="social_links">
        <a href="XXXXXXX" class="twitter"><span class="arrow"><i>Twitter</i></span></a>
        <a href="XXXXXXX" class="facebook"><span class="arrow"><i>Facebook</i></span></a>
        <a href="XXXXXXX" class="flickr"><span class="arrow"><i>Flickr</i></span></a>
        <a href="XXXXXXX" class="youtube"><span class="arrow"><i>YouTube</i></span></a>
        <a href="XXXXXXX" class="google"><span class="arrow"><i>Google+</i></span></a>
      </div>
      <!-- END ABOUT US INFO -->
    </div>
  </div>
  <div class="space"><?php if ($informations) { ?><div class="column">
      <i><?php echo $text_information; ?></i>
        <ul>
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
    </div><?php } ?><div class="column">
      <i><?php echo $text_service; ?></i>
        <ul>
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
    </div><div class="column">
      <i><?php echo $text_extra; ?></i>
        <ul>
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
    </div><div class="column">
      <i><?php echo $text_account; ?></i>
        <ul>
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
    </div></div>
</div>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<div id="powered" class="powered">
  <a class="up" id="up" title="TOP">&nbsp;</a>
  <script><!-- 
    $(function() {
        $('#up').click(
            function (e) {
                $('html, body').animate({scrollTop: '0px'}, 500);
            }
        );
    });
  //--></script>
  <p><?php echo $powered; ?></p>
</div>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<!-- END FOOTER -->
</body>
</html>