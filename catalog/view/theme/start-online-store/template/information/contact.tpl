<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page CONTACT_PAGE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <h1 class="icon_h contact_us"><?php echo $heading_title; ?></h1>
  <?php echo $content_top; ?>
  <div class="space">
    <div class="location">
      <h5><?php echo $text_location; ?></h5>
      <p class="address"><b><?php echo $text_address; ?></b><br /><?php echo $store; ?><br /><?php echo $address; ?></p>
      <?php if ($telephone) { ?><p class="telephone"><b><?php echo $text_telephone; ?></b><br /><?php echo $telephone; ?></p><?php } ?>
      <?php if ($fax) { ?><p class="fax"><b><?php echo $text_fax; ?></b><br /><?php echo $fax; ?></p><?php } ?>
    </div>
    <div class="contact_form">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <h5><?php echo $text_contact; ?></h5>
      <div class="box_input">
        <i class="title"><?php echo $entry_name; ?> <span class="required">*</span></i>
        <div>
          <input type="text" name="name" value="<?php echo $name; ?>" />
        </div>
        <?php if ($error_name) { ?><span class="error error_top"><?php echo $error_name; ?></span><?php } ?>
      </div>
      <div class="box_input">
        <i class="title"><?php echo $entry_email; ?> <span class="required">*</span></i>
        <div>
          <input type="text" name="email" value="<?php echo $email; ?>" />
        </div>
        <?php if ($error_email) { ?><span class="error error_top"><?php echo $error_email; ?></span><?php } ?>
      </div>
       <div class="box_input">
        <i class="title"><?php echo $entry_Phone; ?> <span class="required">*</span></i>
        <div>
          <input type="text" name="Phone" value="<?php echo $Phone; ?>" />
        </div>
        <?php if ($error_Phone) { ?><span class="error error_top"><?php echo $error_Phone; ?></span><?php } ?>
      </div>
      
      <div class="box_input">
        <i class="title"><?php echo $entry_enquiry; ?> <span class="required">*</span></i>
        <div>
          <textarea name="enquiry"><?php echo $enquiry; ?></textarea>
        </div>
        <?php if ($error_enquiry) { ?><span class="error error_top"><?php echo $error_enquiry; ?></span><?php } ?>
      </div>
      <div class="box_input">
        <i class="title"><?php echo $entry_captcha; ?> <span class="required">*</span></i>
        <div>
          <input type="text" name="captcha" value="<?php echo $captcha; ?>" /> <img align="top" src="index.php?route=information/contact/captcha" alt="" />
        </div>
        <?php if ($error_captcha) { ?><span class="error error_top"><?php echo $error_captcha; ?></span><?php } ?>
      </div>
      <div class="box_actions">
        <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
      </div>
      </form>
    </div>
  </div>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>