<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page SITE_MAP_PAGE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <h1 class="icon_h site_map"><?php echo $heading_title; ?></h1>
  <?php echo $content_top; ?>
  <div class="sitemap">
    <div class="left">
      <ul class="list_info_site">
        <?php foreach ($categories as $category_1) { ?>
        <li><a href="<?php echo $category_1['href']; ?>" title="<?php echo $category_1['name']; ?>"><?php echo $category_1['name']; ?></a>
          <?php if ($category_1['children']) { ?>
          <ul>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <li><a href="<?php echo $category_2['href']; ?>" title="<?php echo $category_2['name']; ?>"><?php echo $category_2['name']; ?></a>
              <?php if ($category_2['children']) { ?>
              <ul>
                <?php foreach ($category_2['children'] as $category_3) { ?>
                <li><a href="<?php echo $category_3['href']; ?>" title="<?php echo $category_3['name']; ?>"><?php echo $category_3['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
    </div>
    <div class="right">
      <div class="left">
        <ul class="list_info_site">
          <li class="link_style"><a class="map_special_offers" href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
          <li class="link_style"><a class="map_shopping_cart" href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a></li>
        </ul>
      </div>
      <div class="right">
        <ul class="list_info_site">
          <li class="link_style"><a class="map_search" href="<?php echo $search; ?>"><?php echo $text_search; ?></a></li>
          <li class="link_style"><a class="map_checkout" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
        </ul>
      </div>
      <div class="separator"></div>
      <div class="left">
        <ul class="list_info_site">
          <li><?php echo $text_information; ?>
            <ul>
              <?php foreach ($informations as $information) { ?>
              <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
              <?php } ?>
              <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="right">
        <ul class="list_info_site">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
            <ul>
              <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
              <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
              <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
              <li><a href="<?php echo $history; ?>"><?php echo $text_history; ?></a></li>
              <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>