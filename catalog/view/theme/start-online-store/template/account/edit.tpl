<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_ACCOUNT_EDIT">
<div id="notification"></div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_account_edit"><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div class="box_account">
    <p class="ptitle"><?php echo $text_your_details; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_firstname; ?> <span class="required">*</span></i>
      <div><input type="text" name="firstname" value="<?php echo $firstname; ?>" /><?php if ($error_firstname) { ?><span class="error"><?php echo $error_firstname; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_lastname; ?> <span class="required">*</span></i>
      <div><input type="text" name="lastname" value="<?php echo $lastname; ?>" /><?php if ($error_lastname) { ?><span class="error"><?php echo $error_lastname; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_email; ?> <span class="required">*</span></i>
      <div><input type="text" name="email" value="<?php echo $email; ?>" /><?php if ($error_email) { ?><span class="error"><?php echo $error_email; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_telephone; ?> <span class="required">*</span></i>
      <div><input type="text" name="telephone" value="<?php echo $telephone; ?>" /><?php if ($error_telephone) { ?><span class="error"><?php echo $error_telephone; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_fax; ?></i>
      <div><input type="text" name="fax" value="<?php echo $fax; ?>" /></div>
    </div>
  </div>
  <div class="box_account">
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
      <a href="<?php echo $back; ?>" class="mbut mgray mshadow m_icon_back"><b><?php echo $button_back; ?></b></a>
    </div>
  </div>
  </form>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>