<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_ACCOUNT_VOUCHER">
<div id="notification"></div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_account_voucher"><?php echo $heading_title; ?></h1>
  <p class="pinfo_account"><?php echo $text_description; ?></p>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div class="box_account">
    <div class="box_input">
      <i class="title"><?php echo $entry_to_name; ?> <span class="required">*</span></i>
      <div><input type="text" name="to_name" value="<?php echo $to_name; ?>" /><?php if ($error_to_name) { ?><span class="error"><?php echo $error_to_name; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_to_email; ?> <span class="required">*</span></i>
      <div><input type="text" name="to_email" value="<?php echo $to_email; ?>" /><?php if ($error_to_email) { ?><span class="error"><?php echo $error_to_email; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_from_name; ?> <span class="required">*</span></i>
      <div><input type="text" name="from_name" value="<?php echo $from_name; ?>" /><?php if ($error_from_name) { ?><span class="error"><?php echo $error_from_name; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_from_email; ?> <span class="required">*</span></i>
      <div><input type="text" name="from_email" value="<?php echo $from_email; ?>" /><?php if ($error_from_email) { ?><span class="error"><?php echo $error_from_email; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_theme; ?> <span class="required">*</span></i>
      <div><?php foreach ($voucher_themes as $voucher_theme) { ?>
        <?php if ($voucher_theme['voucher_theme_id'] == $voucher_theme_id) { ?>
        <input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>" id="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>" checked="checked" />
        <label for="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>"><?php echo $voucher_theme['name']; ?></label><br />
        <?php } else { ?>
        <input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>" id="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>" />
        <label for="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>"><?php echo $voucher_theme['name']; ?></label><br />
        <?php } ?>
        <?php } ?>
      </div>
      <?php if ($error_theme) { ?><span class="error error_top"><?php echo $error_theme; ?></span><?php } ?>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_message; ?></i>
      <div><textarea name="message"><?php echo $message; ?></textarea></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_amount; ?> <span class="required">*</span></i>
      <div><input type="text" name="amount" value="<?php echo $amount; ?>" /><?php if ($error_amount) { ?><span class="error"><?php echo $error_amount; ?></span><?php } ?></div>
    </div>
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
      <?php if ($agree) { ?>
      <input type="checkbox" name="agree" value="1" checked="checked" />
      <?php } else { ?>
      <input type="checkbox" name="agree" value="1" />
      <?php } ?>    
      <?php echo $text_agree; ?>
    </div>
  </div>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>