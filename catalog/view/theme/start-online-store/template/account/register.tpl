<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page ACCOUNT_REGISTER">
<div id="notification"></div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h register_account"><?php echo $heading_title; ?></h1>
  <p class="pinfo_account"><?php echo $text_account_already; ?></p>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_your_details; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_firstname; ?> <span class="required">*</span></i>
      <div><input type="text" name="firstname" value="<?php echo $firstname; ?>" /><?php if ($error_firstname) { ?><span class="error"><?php echo $error_firstname; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_lastname; ?> <span class="required">*</span></i>
      <div><input type="text" name="lastname" value="<?php echo $lastname; ?>" /><?php if ($error_lastname) { ?><span class="error"><?php echo $error_lastname; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_email; ?> <span class="required">*</span></i>
      <div><input type="text" name="email" value="<?php echo $email; ?>" /><?php if ($error_email) { ?><span class="error"><?php echo $error_email; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_telephone; ?> <span class="required">*</span></i>
      <div><input type="text" name="telephone" value="<?php echo $telephone; ?>" /><?php if ($error_telephone) { ?><span class="error"><?php echo $error_telephone; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_fax; ?></i>
      <div><input type="text" name="fax" value="<?php echo $fax; ?>" /></div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_your_address; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_company; ?></i>
      <div><input type="text" name="company" value="<?php echo $company; ?>" /></div>
    </div>
    <div class="box_input" style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
      <i class="title"><?php echo $entry_customer_group; ?></i>
      <div>
        <?php foreach ($customer_groups as $customer_group) { ?>
        <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
        <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
        <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
        <br />
        <?php } else { ?>
        <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
        <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
        <br />
        <?php } ?>
        <?php } ?>      
      </div>
    </div>
    <div id="company-id-display" class="box_input">
      <i class="title"><?php echo $entry_company_id; ?> <span id="company-id-required" class="required">*</span></i>
      <div><input type="text" name="company_id" value="<?php echo $company_id; ?>" /><?php if ($error_company_id) { ?><span class="error"><?php echo $error_company_id; ?></span><?php } ?></div>
    </div>
    <div id="tax-id-display" class="box_input">
      <i class="title"><?php echo $entry_tax_id; ?> <span id="tax-id-required" class="required">*</span></i>
      <div><input type="text" name="tax_id" value="<?php echo $tax_id; ?>" /><?php if ($error_tax_id) { ?><span class="error"><?php echo $error_tax_id; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_address_1; ?> <span class="required">*</span></i>
      <div><input type="text" name="address_1" value="<?php echo $address_1; ?>" /><?php if ($error_address_1) { ?><span class="error"><?php echo $error_address_1; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_address_2; ?></i>
      <div><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_city; ?> <span class="required">*</span></i>
      <div><input type="text" name="city" value="<?php echo $city; ?>" /><?php if ($error_city) { ?><span class="error"><?php echo $error_city; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_postcode; ?> <span id="postcode-required" class="required">*</span></i>
      <div><input type="text" name="postcode" value="<?php echo $postcode; ?>" /><?php if ($error_postcode) { ?><span class="error"><?php echo $error_postcode; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_country; ?> <span class="required">*</span></i>
      <div>
        <select name="country_id">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($countries as $country) { ?>
            <?php if ($country['country_id'] == $country_id) { ?>
            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select>
        <?php if ($error_country) { ?><span class="error"><?php echo $error_country; ?></span><?php } ?>
      </div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_zone; ?> <span class="required">*</span></i>
      <div>
        <select name="zone_id">
        </select>
        <?php if ($error_zone) { ?><span class="error"><?php echo $error_zone; ?></span><?php } ?>
      </div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_your_password; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_password; ?> <span class="required">*</span></i>
      <div><input type="password" name="password" value="<?php echo $password; ?>" /><?php if ($error_password) { ?><span class="error"><?php echo $error_password; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_confirm; ?> <span class="required">*</span></i>
      <div><input type="password" name="confirm" value="<?php echo $confirm; ?>" /><?php if ($error_confirm) { ?><span class="error"><?php echo $error_confirm; ?></span><?php } ?></div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_newsletter; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_newsletter; ?></i>
      <div>
        <?php if ($newsletter) { ?>
        <label><input type="radio" name="newsletter" value="1" checked="checked" /></label>
        <?php echo $text_yes; ?>
        <label><input type="radio" name="newsletter" value="0" /></label>
        <?php echo $text_no; ?>
        <?php } else { ?>
        <label><input type="radio" name="newsletter" value="1" /></label>
        <?php echo $text_yes; ?>
        <label><input type="radio" name="newsletter" value="0" checked="checked" /></label>
        <?php echo $text_no; ?>
        <?php } ?>
      </div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <?php if ($text_agree) { ?>
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" /> <?php if ($agree) { ?>
      <input type="checkbox" name="agree" value="1" checked="checked" /><?php } else { ?>
      <input type="checkbox" name="agree" value="1" /><?php } ?>
      <?php echo $text_agree; ?>
    </div>
    <?php } else { ?>
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
    </div>
    <?php } ?>
  </div>
  <!-- END -->
  </form>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
$('input[name=\'customer_group_id\']:checked').live('change', function() {
	var customer_group = [];
	
<?php foreach ($customer_groups as $customer_group) { ?>
	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
<?php } ?>	

	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('#company-id-display').show();
		} else {
			$('#company-id-display').hide();
		}
		
		if (customer_group[this.value]['company_id_required'] == '1') {
			$('#company-id-required').show();
		} else {
			$('#company-id-required').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('#tax-id-display').show();
		} else {
			$('#tax-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_required'] == '1') {
			$('#tax-id-required').show();
		} else {
			$('#tax-id-required').hide();
		}	
	}
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;</span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	width: 640,
	height: 480
});
//--></script> 
<?php echo $footer; ?>