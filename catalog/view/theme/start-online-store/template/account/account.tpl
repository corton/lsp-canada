<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_ACCOUNT">
<div id="notification"></div>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_account"><?php echo $heading_title; ?></h1>
  <div class="box_account">
    <p class="ptitle"><?php echo $text_my_account; ?></p>
    <ul class="list_my_account">
      <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
      <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
      <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
    </ul>
  </div>
  <div class="box_account">
    <p class="ptitle"><?php echo $text_my_orders; ?></p>
    <ul class="list_my_account">
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
      <?php if ($reward) { ?>
      <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
      <?php } ?>
      <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
      <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
    </ul>
  </div>
  <div class="box_account">
    <p class="ptitle"><?php echo $text_my_newsletter; ?></p>
    <ul class="list_my_account">
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>