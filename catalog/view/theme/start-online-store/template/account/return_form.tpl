<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_ACCOUNT_RETURNS">
<div id="notification"></div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_account_returns"><?php echo $heading_title; ?></h1>
  <div class="pinfo_account"><?php echo $text_description; ?></div>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div class="box_account">
    <p class="ptitle"><?php echo $text_order; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_firstname; ?> <span class="required">*</span></i>
      <div><input type="text" name="firstname" value="<?php echo $firstname; ?>" /><?php if ($error_firstname) { ?><span class="error"><?php echo $error_firstname; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_lastname; ?> <span class="required">*</span></i>
      <div><input type="text" name="lastname" value="<?php echo $lastname; ?>" /><?php if ($error_lastname) { ?><span class="error"><?php echo $error_lastname; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_email; ?> <span class="required">*</span></i>
      <div><input type="text" name="email" value="<?php echo $email; ?>" /><?php if ($error_email) { ?><span class="error"><?php echo $error_email; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_telephone; ?> <span class="required">*</span></i>
      <div><input type="text" name="telephone" value="<?php echo $telephone; ?>" /><?php if ($error_telephone) { ?><span class="error"><?php echo $error_telephone; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_order_id; ?> <span class="required">*</span></i>
      <div><input type="text" name="order_id" value="<?php echo $order_id; ?>" /><?php if ($error_order_id) { ?><span class="error"><?php echo $error_order_id; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_date_ordered; ?></i>
      <div><input type="text" name="date_ordered" value="<?php echo $date_ordered; ?>" class="date" /></div>
    </div>
  </div>
  <div class="box_account">
    <p class="ptitle"><?php echo $text_product; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_product; ?> <span class="required">*</span></i>
      <div><input type="text" name="product" value="<?php echo $product; ?>" /><?php if ($error_product) { ?><span class="error"><?php echo $error_product; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_model; ?> <span class="required">*</span></i>
      <div><input type="text" name="model" value="<?php echo $model; ?>" /><?php if ($error_model) { ?><span class="error"><?php echo $error_model; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_quantity; ?></i>
      <div><input type="text" name="quantity" value="<?php echo $quantity; ?>" /></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_reason; ?> <span class="required">*</span></i>
      <div>
        <?php foreach ($return_reasons as $return_reason) { ?>
        <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>
        <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="return-reason-id<?php echo $return_reason['return_reason_id']; ?>" checked="checked" />
        <label for="return-reason-id<?php echo $return_reason['return_reason_id']; ?>"><?php echo $return_reason['name']; ?></label><br />
        <?php } else { ?>
        <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="return-reason-id<?php echo $return_reason['return_reason_id']; ?>" />
        <label for="return-reason-id<?php echo $return_reason['return_reason_id']; ?>"><?php echo $return_reason['name']; ?></label><br />
        <?php  } ?>
        <?php  } ?>
      </div>
      <?php if ($error_reason) { ?><span class="error error_top"><?php echo $error_reason; ?></span><?php } ?>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_opened; ?></i>
      <div>
        <?php if ($opened) { ?>
        <input type="radio" name="opened" value="1" id="opened" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="opened" value="1" id="opened" />
        <?php } ?>
        <label for="opened"><?php echo $text_yes; ?></label>
        <?php if (!$opened) { ?>
        <input type="radio" name="opened" value="0" id="unopened" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="opened" value="0" id="unopened" />
        <?php } ?>
        <label for="unopened"><?php echo $text_no; ?></label>
      </div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_fault_detail; ?></i>
      <div><textarea name="comment"><?php echo $comment; ?></textarea></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_captcha; ?> <span class="required">*</span></i>
      <div><input type="text" name="captcha" value="<?php echo $captcha; ?>" /><?php if ($error_captcha) { ?><span class="error"><?php echo $error_captcha; ?></span><?php } ?><br /><br /><img src="index.php?route=account/return/captcha" alt="" /></div>
    </div>
  </div>
  <div class="box_account">
    <?php if ($text_agree) { ?>
    <div class="box_actions">
        <?php echo $text_agree; ?>
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?>
        <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
        <a href="<?php echo $back; ?>" class="mbut mgray mshadow m_icon_back"><b><?php echo $button_back; ?></b></a>
    </div>
    <?php } else { ?>
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
      <a href="<?php echo $back; ?>" class="mbut mgray mshadow m_icon_back"><b><?php echo $button_back; ?></b></a>
    </div>
    <?php } ?>
  </div>
  </form>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		width: 640,
		height: 480
	});
});
//--></script> 
<?php echo $footer; ?>