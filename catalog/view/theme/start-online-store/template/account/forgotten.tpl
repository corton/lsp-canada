<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page FORGOTTEN_PAGE">
<div id="notification"></div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h forgot_password"><?php echo $heading_title; ?></h1>
  <p class="pinfo_account"><?php echo $text_email; ?></p>
  <div class="box_account">
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <p class="ptitle"><?php echo $text_your_email; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_email; ?> <span class="required">*</span></i>
      <div>
        <input type="text" name="email" value="" />
      </div>
    </div>
    <div class="box_actions">
      <input type="submit" class="mbut mgreen mshadow" value="<?php echo $button_continue; ?>" />
      <a class="mbut mgray mshadow m_icon_back" href="<?php echo $back; ?>"><b><?php echo $button_back; ?></b></a>
    </div>
  </form>
  </div>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>