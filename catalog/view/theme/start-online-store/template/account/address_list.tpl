<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page ACCOUNT_ADDRESS_BOOK">
<div id="notification"></div>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h account_address_book"><?php echo $heading_title; ?></h1>
  <div class="box_account">
    <p class="ptitle"><?php echo $text_address_book; ?></p>
    <?php foreach ($addresses as $result) { ?>
    <div class="box_address_book">
     <p><?php echo $result['address']; ?></p>
     <a href="<?php echo $result['update']; ?>" class="address_book_edit"><?php echo $button_edit; ?></a>
     <a href="<?php echo $result['delete']; ?>" class="address_book_delete"><?php echo $button_delete; ?></a>
    </div>
    <?php } ?>
  </div>
  <div class="box_account">
    <div class="box_actions">
      <a href="<?php echo $insert; ?>" class="mbut mgreen mshadow m_icon_address_book_add"><b><?php echo $button_new_address; ?></b></a>
      <a href="<?php echo $back; ?>" class="mbut mgray mshadow m_icon_back"><b><?php echo $button_back; ?></b></a>
    </div>
  </div>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>