<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_ACCOUNT_NEWSLETTER">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_account_newsletter"><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div class="box_account">
    <p class="ptitle"><?php echo $entry_newsletter; ?></p>
    <div class="box_input">
      <div><?php if ($newsletter) { ?>
        <div class="icon_box yes"><label><input type="radio" name="newsletter" value="1" checked="checked" />
        <?php echo $text_yes; ?></label></div>
        <div class="icon_box no"><label><input type="radio" name="newsletter" value="0" />
        <?php echo $text_no; ?></label></div>
        <?php } else { ?>
        <div class="icon_box yes"><label><input type="radio" name="newsletter" value="1" />
        <?php echo $text_yes; ?></label></div>
        <div class="icon_box no"><label><input type="radio" name="newsletter" value="0" checked="checked" />
        <?php echo $text_no; ?></label></div>
        <?php } ?></div>
    </div>
  </div>
  <div class="box_account">
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
      <a href="<?php echo $back; ?>" class="mbut mgray mshadow m_icon_back"><b><?php echo $button_back; ?></b></a>
    </div>
  </div>
  </form>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>