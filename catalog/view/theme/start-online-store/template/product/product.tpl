<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page PRODUCT_PAGE product-info">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <?php echo $content_top; ?>
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <!-- START ..................................................................... TOP PANEL -->
  <div class="top_panel">
    <!-- start right -->
    <div class="right">
      <div class="image">
        <?php if ($review_status) { ?>
        <div class="mini-reviews">
          <div>
            <div class="rating">
              <i><?php echo $reviews; ?></i>
              <span class="rating_star star<?php echo $rating; ?>">&nbsp;</span>
            </div>
            <a onclick="$('a[href=\'#tab-review\']').trigger('click');" class="write"><?php echo $text_write; ?></a>
          </div>
        </div>
        <?php } ?>
        <?php if ($thumb) { ?>
          <div class="view_zoom_right">
            <a href="<?php echo $popup; ?>" class="cloud-zoom" id="view_zoom" rel="adjustX:-26, adjustY:-5"><img src="<?php echo $popup; ?>" /></a>
          </div>
          <div class="view_zoom_inside">
            <a href="<?php echo $popup; ?>" class="cloud-zoom" id="view_zoom_inside" rel="position:'inside', adjustX:-5, adjustY:-5"><img src="<?php echo $popup; ?>" /></a>
          </div>
          <a href="<?php echo $popup; ?>" class="zoom_colorbox zoom_but" rel="colorbox" title="<?php echo $heading_title; ?>"><span>Zoom</span></a>
        <?php } ?>
      </div>
      <?php if ($thumb || $images) { ?>
      <!-- START IMAGE ADDITIONAL -->
      <div id="image-additional" class="image-additional view_zoom_right">
        <ul class="jcarousel-skin-image-additional">
          <?php if ($thumb) { ?>
          <li><div class="space"><a href="<?php echo $popup; ?>" class="cloud-zoom-gallery" rel="useZoom:'view_zoom', smallImage:'<?php echo $popup; ?>'" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></a></div></li>
          <?php } ?>
          <?php foreach ($images as $image) { ?>
          <li><div class="space"><a href="<?php echo $image['popup']; ?>" class="cloud-zoom-gallery" rel="useZoom:'view_zoom', smallImage:'<?php echo $image['popup']; ?>'" title="<?php echo $heading_title; ?>"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>" /></a></div></li>
          <?php } ?>
        </ul>
      </div>
      <div id="image-additional" class="image-additional view_zoom_inside">
        <ul class="jcarousel-skin-image-additional">
          <?php if ($thumb) { ?>
          <li><div class="space"><a href="<?php echo $popup; ?>" class="cloud-zoom-gallery" rel="useZoom:'view_zoom_inside', smallImage:'<?php echo $popup; ?>'" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></a></div></li>
          <?php } ?>
          <?php foreach ($images as $image) { ?>
          <li><div class="space"><a href="<?php echo $image['popup']; ?>" class="cloud-zoom-gallery" rel="useZoom:'view_zoom_inside', smallImage:'<?php echo $image['popup']; ?>'" title="<?php echo $heading_title; ?>"><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>" /></a></div></li>
          <?php } ?>
        </ul>
      </div>
      <script type="text/javascript">
          jQuery(document).ready(function() {
              jQuery('#image-additional ul').jcarousel({
                  vertical: true,
                  visible: 3,
                  scroll: 3
              });
          });
      </script>
      <!-- END IMAGE ADDITIONAL -->
      <!-- START COLORBOX GALLERY -->
      <div class="colorbox_list_img">
          <?php foreach ($images as $image) { ?>
          <a href="<?php echo $image['popup']; ?>" class="zoom_colorbox" rel="colorbox" title="<?php echo $heading_title; ?>"></a>
          <?php } ?>
      </div>
      <!-- END COLORBOX GALLERY -->
      <?php } else { ?>
        <div class="image-additional">
          <ul>
            <li><div class="space">&nbsp;</div></li>
          </ul>
        </div>
      <?php } ?>
      <div class="share">
        <div class="addthis_default_style">
          <a class="addthis_button_twitter"></a>
          <a class="addthis_button_facebook"></a>
          <a class="addthis_button_compact"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script> 
      </div>
    </div>
    <!-- end right -->
    <!-- start left -->
    <div class="left">
      <h1><?php echo $heading_title; ?></h1>
      <!-- START ... -->
      <div class="left_zone_panel">
        <?php if ($price) { ?>
        <div class="price">
          <?php if (!$special) { ?>
          <?php echo $price; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $special; ?></span>
          <span class="price-old"><?php echo $price; ?><div class="red_icon">%</div></span>
          <?php } ?>
          <?php if ($tax) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span>
          <?php } ?>
        </div>
        <?php if ($points) { ?>
        <span class="price-reward"><?php echo $text_points; ?> <?php echo $points; ?></span>
        <?php } ?>
        <?php if ($discounts) { ?>
        <div class="discount">
          <?php foreach ($discounts as $discount) { ?>
          <span><?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?></span>
          <?php } ?>
        </div>
        <?php } ?>
        <?php } ?>
        <div class="mini-actions">
          <span><a class="wishlist" onclick="addToWishList('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></a></span>
          <span><a class="compare" onclick="addToCompare('<?php echo $product_id; ?>');"><?php echo $button_compare; ?></a></span>
        </div>
      </div>
      <!-- END ... -->
      <!-- START ... -->
      <div class="right_zone_panel">
        <?php if ($options) { ?> <!-- START OPTIONS -->
        <div class="all_options">
          <div class="error_view">
            <?php foreach ($options as $option) { ?>
            	<?php if ($option['type'] == 'select') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'radio') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'checkbox') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'image') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'text') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'textarea') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'file') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'date') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'datetime') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
                <?php if ($option['type'] == 'time') { ?><p id="option-<?php echo $option['product_option_id']; ?>"></p>
                <?php } ?>
            <?php } ?>
          </div>
          <div class="title_all_options"><p><?php echo $text_option; ?></p></div>
          <div class="content_all_options">
          <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <select name="option[<?php echo $option['product_option_id']; ?>]">
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($option['option_value'] as $option_value) { ?>
                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                        <?php if ($option_value['price']) { ?>
                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                        <?php } ?>
                        </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <?php foreach ($option['option_value'] as $option_value) { ?>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?> <?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?></label><br />
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <?php foreach ($option['option_value'] as $option_value) { ?>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?> <?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?></label><br />
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'image') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <table class="option-image">
                        <tbody>
                        <?php foreach ($option['option_value'] as $option_value) { ?>
                        <tr>
                            <td style="width:1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
                            <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                            <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?> <?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?></label></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <textarea name="option[<?php echo $option['product_option_id']; ?>]"><?php echo $option['option_value']; ?></textarea>
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="mbut morange mglossy">
                    <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
                </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                <span><?php if ($option['required']) { ?><i class="required">* </i><?php } ?><?php echo $option['name']; ?>:</span>
                <div>
                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
                </div>
            </div>
            <?php } ?>
          <?php } ?>
          </div>
        </div>
        <?php } ?> <!-- END OPTIONS -->
        <div class="cart">
          <?php if ($minimum > 1) { ?><span class="minimum"><?php echo $text_minimum; ?></span><?php } ?>
          <div class="qty"><i><?php echo $text_qty; ?></i> <input type="text" name="quantity" size="2" value="<?php echo $minimum; ?>" /><input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" /></div>
          <input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="add_to_cart" />
        </div>          
        <div class="description">
          <?php if ($manufacturer) { ?>
          <p><i><?php echo $text_manufacturer; ?></i> <a href="<?php echo $manufacturers; ?>" title="<?php echo $manufacturer; ?>"><?php echo $manufacturer; ?></a></p>
          <?php } ?>
          <p><i><?php echo $text_model; ?></i> <?php echo $model; ?></p>
          <?php if ($reward) { ?>
          <p><i><?php echo $text_reward; ?></i> <?php echo $reward; ?></p>
          <?php } ?>
          <p><i><?php echo $text_stock; ?></i> <?php echo $stock; ?></p>
        </div>          
      </div>
      <!-- END ... -->
    </div>
    <!-- end left -->
  </div>
  <!-- END ..................................................................... TOP PANEL -->
  <div class="separator"><div>&nbsp;</div></div>
  <!-- START ..................................................................... BOTTOM PANEL -->
  <div class="bottom_panel">
    <?php if ($review_status) { ?><div class="show_view_review"></div><?php } ?>
    <!-- START ALL TAB -->
    <div class="left_bottom_panel">
      <div id="tabs" class="tabs">
        <a href="#tab-description"><?php echo $tab_description; ?></a>
        <?php if ($attribute_groups) { ?>
        <a href="#tab-attribute"><?php echo $tab_attribute; ?></a>
        <?php } ?>
        <?php if ($review_status) { ?>
        <a href="#tab-review"><?php echo $text_write; ?></a>
        <?php } ?>
      </div>
      <div class="space_content_tab">
        <?php if ($products) { ?>
        <!-- START RIGHT SPACE -->
        <div class="right_space">
          <!-- START #tab-related -->
          <div class="tab-related">
            <div class="title"><b><?php echo $tab_related; ?> <?php echo count($products); ?></b></div>
            <div id="carousel_related">
              <div class="box-sbr">
                <ul class="jcarousel-skin-related">
                  <?php foreach ($products as $product) { ?>
                  <li>
                    <div class="small sbr_right">
                      <h3><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a></h3>
                      <div class="image">
                        <?php if ($product['thumb']) { ?>
                        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                          <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
                        </a>
                        <?php } ?>
                      </div>
                      <div class="price">
                      <?php if ($product['price']) { ?>
                        <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                        <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span>
                        <span class="price-old"><?php echo $product['price']; ?></span>
                        <?php } ?>
                      <?php } ?>
                      </div>
                      <?php if ($product['rating']) { ?>
                      <div class="rating">
                        <span class="rating_star star<?php echo $product['rating']; ?>">&nbsp;</span>
                      </div>
                      <?php } ?>
                    </div>
                  </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('#carousel_related ul').jcarousel({
                    vertical: true,
                    visible: 3,
                    scroll: 3
                });
            });
            </script>
            </div>
          <!-- END #tab-related -->
        </div>
        <?php } ?>
        <!-- END RIGHT SPACE -->
        <!-- START LEFT SPACE -->
        <div class="left_space">
          <!-- START #tab-description -->
          <div id="tab-description" class="tab-description">
            <?php echo $description; ?>
          </div>
          <!-- END #tab-description -->
          <?php if ($attribute_groups) { ?>
          <!-- START #tab-attribute -->
          <div id="tab-attribute">
            <?php foreach ($attribute_groups as $attribute_group) { ?>
            <table class="tab-attribute">
              <thead><tr class="group"><td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td></tr></thead>
              <tbody>
                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                <tr>
                  <td class="name"></strong><?php echo $attribute['name']; ?></strong></td>
                  <td class="text"><?php echo $attribute['text']; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
          <!-- END #tab-attribute -->
          <?php } ?>
          <!-- START #tab-review -->
          <?php if ($review_status) { ?>
          <div id="tab-review" class="tab-review">
            <p class="title-review"><?php echo $text_write; ?></p>
            <div class="box_input">
              <i class="title"><?php echo $entry_name; ?> <span class="required">*</span></i>
              <div>
                <input type="text" name="name" value="" />
              </div>
            </div>
            <div class="box_input">
              <i class="title"><?php echo $entry_review; ?> <span class="required">*</span></i>
              <div>
                <textarea name="text"></textarea>
                <small><?php echo $text_note; ?></small>
              </div>
            </div>
            <div class="box_input">
              <i class="title"><?php echo $entry_rating; ?> <span class="required">*</span></i>
              <div class="rating_star_tab-review">
                <label><span class="rating_star star1"><input type="radio" name="rating" value="1" /><?php echo $entry_bad; ?></span></label>
                <label><span class="rating_star star2"><input type="radio" name="rating" value="2" /></span></label>
                <label><span class="rating_star star3"><input type="radio" name="rating" value="3" /></span></label>
                <label><span class="rating_star star4"><input type="radio" name="rating" value="4" /></span></label>
                <label><span class="rating_star star5"><input type="radio" name="rating" value="5" /><?php echo $entry_good; ?></span></label>
              </div>
            </div>
            <div class="box_input">
              <i class="title"><?php echo $entry_captcha; ?> <span class="required">*</span></i>
              <div>
                <input type="text" name="captcha" value="" class="captcha" /> <img align="top" src="index.php?route=product/product/captcha" alt="" id="captcha" />
              </div>
            </div>
            <div id="review-title"></div>
            <div class="box_actions" id="review-title">
              <a id="button-review" class="mbut mgreen mshadow"><b><?php echo $button_continue; ?></b></a>
            </div>
          </div>
          <?php } ?>
          <!-- END #tab-review -->
        </div>
        <!-- END LEFT SPACE -->
        <?php if ($tags) { ?>
        <div class="tags">
          <p><?php echo $text_tags; ?>
          <?php for ($i = 0; $i < count($tags); $i++) { ?>
          <?php if ($i < (count($tags) - 1)) { ?>
          <a href="<?php echo $tags[$i]['href']; ?>" title="<?php echo $tags[$i]['tag']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
          <?php } else { ?>
          <a href="<?php echo $tags[$i]['href']; ?>" title="<?php echo $tags[$i]['tag']; ?>"><?php echo $tags[$i]['tag']; ?></a>
          <?php } ?>
          <?php } ?>
          </p>
        </div>
        <?php } ?>
      </div>
    </div>
    <!-- END ALL TAB -->
    <?php if ($review_status) { ?>
    <!-- START VIEW REVIEW -->
    <div class="right_bottom_panel">
    <div class="box">
      <div class="box-heading">
        <strong class="title"><?php if ($manufacturer) { ?><?php echo $manufacturer; ?> <?php } ?><?php echo $model; ?> <?php echo $tab_review; ?></strong>
      </div>
      <div class="box-content">
        <div class="box-view-review">
          <div id="review"></div>
        </div>
      </div>
    </div>
    </div>
    <!-- END VIEW REVIEW -->
    <?php } ?>
  </div>
  <!-- END ..................................................................... BOTTOM PANEL -->
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
$('.zoom_colorbox').colorbox({
	overlayClose: true,
	opacity: 0.3
});
//--></script> 
<script type="text/javascript"><!--
$('#button-cart').bind('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
if ($.browser.msie && $.browser.version == 6) {
	$('.date, .datetime, .time').bgIframe();
}

$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
	dateFormat: 'yy-mm-dd',
	timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script>
<?php echo $footer; ?>