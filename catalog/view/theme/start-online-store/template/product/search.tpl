<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page SEARCH_PAGE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <h1 class="icon_h icon_h_search"><?php echo $heading_title; ?></h1>
  <?php echo $content_top; ?>
  <!-- START GENERAL BOX - Search Criteria -->
  <div class="box">
    <div class="box-heading"><strong class="title"><?php echo $text_critea; ?></strong></div>
    <div class="box-content">
      <div class="box_search_criteria">
        <span><?php echo $entry_search; ?></span>
        <div class="left">
          <?php if ($search) { ?>
          <input type="text" name="search" value="<?php echo $search; ?>" />
          <?php } else { ?>
          <input type="text" name="search" value="<?php echo $search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" />
          <?php } ?>
          <select name="category_id">
            <option value="0"><?php echo $text_category; ?></option>
            <?php foreach ($categories as $category_1) { ?>
            <?php if ($category_1['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if ($category_2['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">- <?php echo $category_2['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_2['category_id']; ?>">- <?php echo $category_2['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if ($category_3['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">-- <?php echo $category_3['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_3['category_id']; ?>">-- <?php echo $category_3['name']; ?></option>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </select>
          <p><label for="sub_category">
          <?php if ($sub_category) { ?>
          <input type="checkbox" name="sub_category" value="1" id="sub_category" checked="checked" />
          <?php } else { ?>
          <input type="checkbox" name="sub_category" value="1" id="sub_category" />
          <?php } ?>
          <?php echo $text_sub_category; ?></label></p>
          <p><label for="description">
          <?php if ($description) { ?>
          <input type="checkbox" name="description" value="1" id="description" checked="checked" />
          <?php } else { ?>
          <input type="checkbox" name="description" value="1" id="description" />
          <?php } ?>
          <?php echo $entry_description; ?></label></p>
        </div>
        <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="mbut morange mshadow" />
      </div>
    </div>
  </div>
  <!-- END GENERAL BOX - Search Criteria -->
  <span class="meeting"><?php echo $text_search; ?></span>
  <?php if ($products) { ?>
  <!-- START PRODUCT FILTER -->
  <div class="product-filter">
    <div class="product-compare"><a href="<?php echo $compare; ?>"><span id="compare-total"><?php echo $text_compare; ?></span></a></div>
    <div class="limit">
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="sort">
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>
  <!-- END PRODUCT FILTER -->
  <div class="product-grid-sm">
  <?php foreach ($products as $product) { ?>
    <!-- START ITEM -->
    <div class="grid-sm">
      <h3><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a></h3>
      <div class="description">
        <h4><?php echo $product['description']; ?></h4>
      </div>
      <div class="image">
        <!-- START -->
        <?php if ($product['rating']) { ?>
        <div class="mini-reviews">
          <div>
            <div class="rating">
              <i><?php echo $product['reviews']; ?></i>
              <span class="rating_star star<?php echo $product['rating']; ?>">&nbsp;</span>
            </div>
          </div>
        </div>
        <?php } ?>
        <!-- END -->
        <?php if ($product['thumb']) { ?><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>" ><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a><?php } ?>
        <?php if ($product['special']) { ?><span class="red_icon">%</span><?php } ?>
      </div>
      <div class="panel">
        <div class="price">
          <?php if ($product['price']) { ?>
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span>
          <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
          <?php } ?>
        </div>
        <div class="mini-actions">
          <span><a class="wishlist" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></span>
          <span><a class="compare" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></a></span>
        </div>
        <div class="cart">
          <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" />
          <div class="info"><a href="<?php echo $product['href']; ?>"><span><i>Info</i></span>&nbsp;</a></div>
        </div>
      </div>
    </div>
    <!-- END ITEM -->
  <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } else { ?>
  <div class="sos_empty"><?php echo $text_empty; ?></div>
  <?php }?>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
$('#content input[name=\'search\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').bind('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').attr('disabled', 'disabled');
		$('input[name=\'sub_category\']').removeAttr('checked');
	} else {
		$('input[name=\'sub_category\']').removeAttr('disabled');
	}
});

$('select[name=\'category_id\']').trigger('change');

$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';
	
	var search = $('#content input[name=\'search\']').attr('value');
	
	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').attr('value');
	
	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}
	
	var sub_category = $('#content input[name=\'sub_category\']:checked').attr('value');
	
	if (sub_category) {
		url += '&sub_category=true';
	}
		
	var filter_description = $('#content input[name=\'description\']:checked').attr('value');
	
	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});
//--></script> 
<?php echo $footer; ?>