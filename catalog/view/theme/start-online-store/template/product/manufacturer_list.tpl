<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page BRAND_PAGE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <h1 class="icon_h manufacturer"><?php echo $heading_title; ?></h1>
  <?php echo $content_top; ?>
  <?php if ($categories) { ?>
  <!-- START BRAND FILTER -->
  <div class="brand-filter">
    <b><?php echo $text_index; ?></b>
    <?php foreach ($categories as $category) { ?>
    <a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
    <?php } ?>
  </div>
  <!-- END BRAND FILTER -->
  <!-- START BRAND LIST -->
  <?php foreach ($categories as $category) { ?>
  <div class="manufacturer-list">
    <div class="anchor"><a id="<?php echo $category['name']; ?>"></a><span><?php echo $category['name']; ?></span></div>
    <ul>
      <?php if ($category['manufacturer']) { ?>
      <?php for ($i = 0; $i < count($category['manufacturer']);) { ?>
        <?php $j = $i + ceil(count($category['manufacturer']) / 4); ?>
        <?php for (; $i < $j; $i++) { ?>
        <?php if (isset($category['manufacturer'][$i])) { ?>
        <li><a href="<?php echo $category['manufacturer'][$i]['href']; ?>"><?php echo $category['manufacturer'][$i]['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      <?php } ?>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>
  <!-- END BRAND LIST -->
  <?php } else { ?>
  <div class="sos_info sos_empty"><p><?php echo $text_empty; ?></p></div>
  <div class="buttons">
    <div class="right"><a class="mbut mgray mshadow" href="<?php echo $continue; ?>"><b><?php echo $button_continue; ?></b></a></div>
  </div>
  <?php }?>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>