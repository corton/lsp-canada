<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page BRAND_PAGE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <h1><?php echo $heading_title; ?></h1>
  <?php echo $content_top; ?>
  <?php if ($products) { ?>
  <!-- START PRODUCT FILTER -->
  <div class="product-filter">
    <div class="product-compare"><a href="<?php echo $compare; ?>"><span id="compare-total"><?php echo $text_compare; ?></span></a></div>
    <div class="limit">
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="sort">
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>
  <!-- END PRODUCT FILTER -->
  <div class="product-grid-sm">
  <?php foreach ($products as $product) { ?>
    <!-- START ITEM -->
    <div class="grid-sm">
      <h3><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a></h3>
      <div class="description">
        <h4><?php echo $product['description']; ?></h4>
      </div>
      <div class="image">
        <!-- START -->
        <?php if ($product['rating']) { ?>
        <div class="mini-reviews">
          <div>
            <div class="rating">
              <i><?php echo $product['reviews']; ?></i>
              <span class="rating_star star<?php echo $product['rating']; ?>">&nbsp;</span>
            </div>
          </div>
        </div>
        <?php } ?>
        <!-- END -->
        <?php if ($product['thumb']) { ?><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>" ><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a><?php } ?>
        <?php if ($product['special']) { ?><span class="red_icon">%</span><?php } ?>
      </div>
      <div class="panel">
        <div class="price">
          <?php if ($product['price']) { ?>
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span>
          <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
          <?php } ?>
        </div>
        <div class="mini-actions">
          <span><a class="wishlist" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></span>
          <span><a class="compare" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></a></span>
        </div>
        <div class="cart">
          <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" />
          <div class="info"><a href="<?php echo $product['href']; ?>"><span><i>Info</i></span>&nbsp;</a></div>
        </div>
      </div>
    </div>
    <!-- END ITEM -->
  <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } else { ?>
  <div class="sos_info sos_empty"><p><?php echo $text_empty; ?></p></div>
  <?php }?>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>