<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page CATEGORY_PAGE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1><?php echo $heading_title; ?></h1>
  <?php if ($thumb || $description) { ?>
  <!-- START CATEGORY INFO -->
  <div class="category-info">
    <?php if ($thumb) { ?>
    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
    <?php } ?>
    <?php if ($description) { ?>
    <?php echo $description; ?>
    <?php } ?>
  </div>
  <!-- START CATEGORY INFO -->
  <?php } ?>
  <?php if ($categories) { ?>
  <!-- START CATEGORY LIST -->
  <div class="category-list-arrow">&nbsp;</div>
  <div class="category-list">
    <?php if (count($categories) <= 5) { ?>
    <ul>
      <?php foreach ($categories as $category) { ?>
      <li><a href="<?php echo $category['href']; ?>" title="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a></li>
      <?php } ?>
    </ul>
    <?php } else { ?>
    <?php for ($i = 0; $i < count($categories);) { ?>
    <ul>
      <?php $j = $i + ceil(count($categories) / 4); ?>
      <?php for (; $i < $j; $i++) { ?>
      <?php if (isset($categories[$i])) { ?>
      <li><a href="<?php echo $categories[$i]['href']; ?>" title="<?php echo $categories[$i]['name']; ?>"><?php echo $categories[$i]['name']; ?></a></li>
      <?php } ?>
      <?php } ?>
    </ul>
    <?php } ?>
    <?php } ?>
  </div>
  <!-- START CATEGORY LIST -->
  <?php } ?>
  <?php if ($products) { ?>
  <!-- START PRODUCT FILTER -->
  <div class="product-filter">
    <div class="product-compare"><a href="<?php echo $compare; ?>"><span id="compare-total"><?php echo $text_compare; ?></span></a></div>
    <div class="display">
      <div class="sqactive"><a class="sqlist sqlist_active"></a></div>
      <div><a class="sqgrid" onclick="display('grid');"><span><i><?php echo $text_grid; ?></i></span></a></div>
    </div>
    <div class="limit">
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="sort">
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>
  <!-- END PRODUCT FILTER -->
  <!-- START PRODUCT LIST or GRID -->
  <div class="product-list">
    <?php foreach ($products as $product) { ?>
    <!-- START ITEM LIST -->
    <div>
      <div class="image">
        <?php if ($product['thumb']) { ?>
        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
        <?php } ?>
      </div>
      <div class="panel">
        <div class="price">
        <?php if ($product['price']) { ?>
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span>
          <span class="price-old">
            <?php echo $product['price']; ?>
            <div class="red_icon">%</div>
          </span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        <?php } ?>
        </div>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <i><?php echo $product['reviews']; ?></i>
          <span class="rating_star star<?php echo $product['rating']; ?>">&nbsp;</span>
        </div>
        <?php } ?>
        <div class="mini-actions">
          <span><a class="wishlist" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></span>
          <span><a class="compare" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></span>
        </div>
      </div>
      <div class="panel_info">
        <div class="name"><h3><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a></h3></div>
        <div class="description"><h4><?php echo $product['description']; ?></h4></div>
        <div class="cart">
          <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" />
          <div class="info"><a href="<?php echo $product['href']; ?>"><span><i>Info</i></span>&nbsp;</a></div>
        </div>
      </div>
    </div>
    <!-- END ITEM -->
    <?php } ?>
  </div>
  <!-- START PRODUCT LIST or GRID -->
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>
  <?php if (!$categories && !$products) { ?>
  <div class="sos_empty"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="mbut mgray mshadow"><b><?php echo $button_continue; ?></b></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			html  = '<div class="image">' + $(element).find('.image').html() + '</div>';
			html += '<div class="panel">';
			html += '  <div class="price">' + $(element).find('.price').html() + '</div>';
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
			
			html += '  <div class="mini-actions">' + $(element).find('.mini-actions').html() + '</div>';
			html += '</div>';			
			html += '<div class="panel_info">';			
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '</div>';			

			$(element).html(html);
		});		
		
		$('.display').html('<div class="sqactive"><a class="sqlist sqlist_active"></a></div><div><a class="sqgrid" onclick="display(\'grid\');"><span><i><?php echo $text_grid; ?></i></span></a></div>');
		
		$.cookie('display', 'list'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			html = '';

			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';
			html += '<div class="image">' + $(element).find('.image').html() + '</div>';
			html += '<div class="price">' + $(element).find('.price').html() + '</div>';
			html += '<div class="content_info">';			
			html += '<div class="panel_info">';			
			html += '<div class="small_image">' + $(element).find('.image').html() + '</div>';
			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
			
			html += '<div class="space_rating">';			
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
			
			html += '</div>';
						
			html += '<div class="mini-actions">' + $(element).find('.mini-actions').html() + '</div>';
			html += '</div>';			
			html += '</div>';
						
			$(element).html(html);
		});	
					
		$('.display').html('<div><a class="sqlist" onclick="display(\'list\');"><span><i><?php echo $text_list; ?></i></span></a></div><div class="sqactive"><a class="sqgrid sqgrid_active"></a></div>');
		
		$.cookie('display', 'grid');
	}
}

view = $.cookie('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script>
<?php echo $footer; ?>