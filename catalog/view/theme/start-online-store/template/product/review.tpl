<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="review-list">
  <div class="right">
    <span><b><?php echo $review['author']; ?></b> <?php echo $text_on; ?><br /><?php echo $review['date_added']; ?></span>
    <span class="rating_star star<?php echo $review['rating'] ?>">&nbsp;</span>
  </div>
  <p><?php echo $review['text']; ?></p>
</div>    
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>       
<div class="sos_empty"><?php echo $text_no_reviews; ?></div>
<?php } ?>