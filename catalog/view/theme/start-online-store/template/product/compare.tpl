<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page COMPARISON_PAGE">
<div id="notification"></div>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <h1 class="icon_h product_comparison"><?php echo $heading_title; ?></h1>
  <?php echo $content_top; ?>
  <?php if ($products) { ?>
  <table class="compare-info">
    <thead>
      <tr class="title-compare">
        <td colspan="<?php echo count($products) + 1; ?>"><p><?php echo $text_product; ?></p></td>
      </tr>
    </thead>
    <tbody>
      <tr class="title">
        <td><?php echo $text_name; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><a href="<?php echo $products[$product['product_id']]['href']; ?>"><?php echo $products[$product['product_id']]['name']; ?></a></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_image; ?></td>
        <?php foreach ($products as $product) { ?>
        <td class="image">
          <?php if ($products[$product['product_id']]['thumb']) { ?>
          <a href="<?php echo $products[$product['product_id']]['href']; ?>"><img src="<?php echo $products[$product['product_id']]['thumb']; ?>" alt="<?php echo $products[$product['product_id']]['name']; ?>" /></a>
          <?php } ?>
        </td>
        <?php } ?>
      </tr>
      <tr class="price">
       <td><?php echo $text_price; ?></td>
       <?php foreach ($products as $product) { ?>
       <td><?php if ($products[$product['product_id']]['price']) { ?>
         <?php if (!$products[$product['product_id']]['special']) { ?>
         <?php echo $products[$product['product_id']]['price']; ?>
         <?php } else { ?>
         <span class="price-new"><?php echo $products[$product['product_id']]['special']; ?></span><span class="price-old"><?php echo $products[$product['product_id']]['price']; ?></span>
         <?php } ?>
         <?php } ?></td>
       <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_model; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php echo $products[$product['product_id']]['model']; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_manufacturer; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php echo $products[$product['product_id']]['manufacturer']; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_availability; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php echo $products[$product['product_id']]['availability']; ?></td>
        <?php } ?>
      </tr>
      <tr class="rating">
        <td><?php echo $text_rating; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><i><?php echo $products[$product['product_id']]['reviews']; ?></i>
        <span class="rating_star star<?php echo $products[$product['product_id']]['rating']; ?>">&nbsp;</span>
        </td><?php } ?>
      </tr>
      <tr class="summary">
        <td><?php echo $text_summary; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php echo $products[$product['product_id']]['description']; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_weight; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php echo $products[$product['product_id']]['weight']; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td><?php echo $text_dimension; ?></td>
        <?php foreach ($products as $product) { ?>
        <td><?php echo $products[$product['product_id']]['length']; ?> x <?php echo $products[$product['product_id']]['width']; ?> x <?php echo $products[$product['product_id']]['height']; ?></td>
        <?php } ?>
      </tr>
    </tbody>
    <?php foreach ($attribute_groups as $attribute_group) { ?>
    <thead>
     <tr class="title-compare">
       <td class="title-attribute" colspan="<?php echo count($products) + 1; ?>"><p><?php echo $attribute_group['name']; ?></p></td>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
      <tr>
        <td><?php echo $attribute['name']; ?></td>
        <?php foreach ($products as $product) { ?>
        <?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>
        <td><?php echo $products[$product['product_id']]['attribute'][$key]; ?></td>
        <?php } else { ?>
        <td></td>
        <?php } ?>
        <?php } ?>
      </tr>
    <?php } ?>
    </tbody>
    <?php } ?>
    <tfoot>
      <tr>
        <td></td>
        <?php foreach ($products as $product) { ?>
        <td>
        <input type="button" value="<?php echo $button_cart; ?>" class="mbut mgreen mglossy" onclick="addToCart('<?php echo $product['product_id']; ?>');" /><br />
        <a class="remove" href="<?php echo $product['remove']; ?>"><?php echo $button_remove; ?></a>
        </td>
        <?php } ?>
      </tr>
    </tfoot>
  </table>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="mbut mgray mshadow"><b><?php echo $button_continue; ?></b></a></div>
  </div>
  <?php } else { ?>
  <div class="sos_empty"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="mbut mgray mshadow"><b><?php echo $button_continue; ?></b></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>