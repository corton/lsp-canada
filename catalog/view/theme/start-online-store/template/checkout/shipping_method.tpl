<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="left-checkout-content"><!-- START LEFT -->
<?php if ($shipping_methods) { ?>
<p><?php echo $text_shipping_method; ?></p>
<table class="radio">
  <?php foreach ($shipping_methods as $shipping_method) { ?>
  <tr>
    <td class="highlighttitle" colspan="3"><b><?php echo $shipping_method['title']; ?></b></td>
  </tr>
  <?php if (!$shipping_method['error']) { ?>
  <?php foreach ($shipping_method['quote'] as $quote) { ?>
  <tr class="highlight">
    <td><?php if ($quote['code'] == $code || !$code) { ?>
      <?php $code = $quote['code']; ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
      <?php } ?></td>
    <td><label for="<?php echo $quote['code']; ?>"><?php echo $quote['title']; ?></label></td>
    <td class="highlightprice" style="text-align:right;"><label for="<?php echo $quote['code']; ?>"><b><?php echo $quote['text']; ?></b></label></td>
  </tr>
  <?php } ?>
  <?php } else { ?>
  <tr>
    <td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
  </tr>
  <?php } ?>
  <?php } ?>
</table>
<br />
<?php } ?>
</div><!-- END LEFT -->
<div class="right-checkout-content"><!-- START RIGHT -->
  <b class="title_comment"><?php echo $text_comments; ?></b>
  <textarea name="comment"><?php echo $comment; ?></textarea>
</div><!-- END RIGHT -->
<!-- START -->
<div class="box_account">
    <div class="box_actions">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="mbut mgreen mshadow" />
    </div>
</div>
<!-- END -->
