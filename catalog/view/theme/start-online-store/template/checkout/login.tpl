<div class="box_account ACCOUNT_LOGIN">
<p class="ptitle"><?php echo $text_returning_customer; ?></p>
<div class="left" id="login"><!-- start left -->
  <div class="box_input">
    <i class="title"><?php echo $entry_email; ?></i>
    <div>
      <input type="text" name="email" value="" />
    </div>
  </div>
  <div class="box_input">
    <i class="title"><?php echo $entry_password; ?></i>
    <div>
      <input type="password" name="password" value="" />
    </div>
  </div>
  <div class="box_actions">
    <input type="submit" class="mbut mgreen mshadow" value="<?php echo $button_login; ?>" id="button-login" />
    <a class="link_right" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
  </div>
</div><!-- end left -->
<div class="right"><!-- start right -->
  <div class="new_customer">
    <strong><?php echo $text_new_customer; ?></strong>
    <div class="space_text"><?php echo $text_register_account; ?></div>
    <div class="bottom">
      <div class="le">
        <span class="le-title"><?php echo $text_checkout; ?></span>
        <label for="register">
          <?php if ($account == 'register') { ?>
          <input type="radio" name="account" value="register" id="register" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="account" value="register" id="register" />
          <?php } ?>
          <?php echo $text_register; ?>
        </label>
        <?php if ($guest_checkout) { ?>
        <label for="guest">
          <?php if ($account == 'guest') { ?>
          <input type="radio" name="account" value="guest" id="guest" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="account" value="guest" id="guest" />
          <?php } ?>
          <?php echo $text_guest; ?>
        </label>
        <?php } ?>
      </div>
      <div class="ri"><input type="button" value="<?php echo $button_continue; ?>" id="button-account" class="mbut mblack mshadow" /></div>
    </div>
  </div>
</div><!-- end right -->
</div>