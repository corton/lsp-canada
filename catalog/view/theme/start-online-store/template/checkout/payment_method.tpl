<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="left-checkout-content"><!-- START LEFT -->
<?php if ($payment_methods) { ?>
<p><?php echo $text_payment_method; ?></p>
<table class="radio" style="margin-top:5px;">
  <?php foreach ($payment_methods as $payment_method) { ?>
  <tr class="highlight">
    <td><?php if ($payment_method['code'] == $code || !$code) { ?>
      <?php $code = $payment_method['code']; ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
      <?php } ?></td>
    <td><label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label></td>
  </tr>
  <?php } ?>
</table>
<br />
<i>You do not need a Paypal account to make a payment. Please continue to enter your credit card information.</i>
<?php } ?>
</div><!-- END LEFT -->
<div class="right-checkout-content"><!-- START RIGHT -->
  <b class="title_comment"><?php echo $text_comments; ?></b>
  <textarea name="comment"><?php echo $comment; ?></textarea>
</div><!-- END RIGHT -->
<!-- START -->
<div class="box_account">
	<div class="box_actions">
        <?php if ($text_agree) { ?>
        <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="mbut mgreen mshadow" />
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?>
        <?php echo $text_agree; ?>
    </div>
    <?php } else { ?>
    <div class="box_actions">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="mbut mgreen mshadow" />
    </div>
    <?php } ?>
</div>
<!-- END -->
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	width: 640,
	height: 480
});
//--></script>