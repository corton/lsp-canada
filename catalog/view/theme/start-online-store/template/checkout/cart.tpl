<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page SHOPPING_CART">
<div id="notification"></div>
<?php if ($attention) { ?>
<div class="attention"><?php echo $attention; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h shopping_cart"><?php echo $heading_title; ?> <?php if ($weight) { ?><span class="cart_weight"><?php echo $weight; ?></span><?php } ?></h1>
      <!-- START CART LEFT -->
      <div class="cart_left">
        <div class="cart_header">
          <span class="image"><?php echo $column_image; ?></span>
          <span class="unit_price"><?php echo $column_price; ?></span>
          <span class="total"><?php echo $column_total; ?></span>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <?php foreach ($products as $product) { ?>
        <div class="cart_content">
          <div class="image">
            <?php if ($product['thumb']) { ?><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a><?php } ?>
          </div>
          <div class="product">
            <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a> <?php if (!$product['stock']) { ?><span class="stock">***</span><?php } ?>
            <p class="model"><i><?php echo $column_model; ?>:</i> <?php echo $product['model']; ?></p>
            <div class="option">
              <?php foreach ($product['option'] as $option) { ?>
              <span>- <?php echo $option['name']; ?>: <?php echo $option['value']; ?></span>
              <?php } ?>
            </div>
            <?php if ($product['reward']) { ?><span class="reward"><?php echo $product['reward']; ?></span><?php } ?>
          </div>
          <div class="unit_price"><?php echo $product['price']; ?></div>
          <div class="total"><?php echo $product['total']; ?></div>
          <div class="quantity">
            <div class="content">
              <span class="qty"><?php echo $column_quantity; ?></span>
              <input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />
              <div class="cart_options update"><span><i><?php echo $button_update; ?></i></span><input type="image" src="catalog/view/theme/start-online-store/stylesheet/img/icon16/cart_options_update.png" alt="<?php echo $button_update; ?>" title="<?php echo $button_update; ?>" /></div>
              <div class="cart_options remove"><span><i><?php echo $button_remove; ?></i></span><a href="<?php echo $product['remove']; ?>" title="<?php echo $button_remove; ?>">&nbsp;</a></div>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php foreach ($vouchers as $vouchers) { ?>
        <div class="cart_content cart_vouchers">
          <div class="image"></div>
          <div class="product">
            <span class="title_vouchers"><?php echo $vouchers['description']; ?></span>
          </div>
          <div class="unit_price"><?php echo $vouchers['amount']; ?></div>
          <div class="total"><?php echo $vouchers['amount']; ?></div>
          <div class="quantity">
            <div class="content">
              <span class="qty"><?php echo $column_quantity; ?></span>
              <input type="text" class="disabled_vouchers" name="" value="1" size="1" disabled="disabled" />
              <div class="cart_options remove"><span><i><?php echo $button_remove; ?></i></span><a href="<?php echo $vouchers['remove']; ?>" title="<?php echo $button_remove; ?>">&nbsp;</a></div>
            </div>
          </div>
        </div>
        <?php } ?>
        </form>
        <a href="<?php echo $continue; ?>" class="mbut mblack mshadow m_icon_back"><b><?php echo $button_shopping; ?></b></a>
      </div>
      <!-- END CART LEFT -->
      <!-- START CART RIGHT -->
      <div class="cart_right">
        <?php if ($coupon_status || $voucher_status || $reward_status || $shipping_status) { ?>
        <div class="options">
          <p><b><?php echo $text_next; ?></b><?php echo $text_next_choice; ?></p>
        </div> 
        <table class="radio">
          <tbody>
            <?php if ($coupon_status) { ?>
            <tr class="highlight">
              <td><?php if ($next == 'coupon') { ?>
              <input type="radio" name="next" value="coupon" id="use_coupon" checked="checked" />
              <?php } else { ?>
              <input type="radio" name="next" value="coupon" id="use_coupon" />
              <?php } ?></td>
              <td><label for="use_coupon"><?php echo $text_use_coupon; ?></label></td>
            </tr>
            <?php } ?>
            <?php if ($voucher_status) { ?>
            <tr class="highlight">
              <td><?php if ($next == 'voucher') { ?>
                <input type="radio" name="next" value="voucher" id="use_voucher" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="next" value="voucher" id="use_voucher" />
                <?php } ?></td>
              <td><label for="use_voucher"><?php echo $text_use_voucher; ?></label></td>
            </tr>
            <?php } ?>
            <?php if ($reward_status) { ?>
            <tr class="highlight">
              <td><?php if ($next == 'reward') { ?>
                <input type="radio" name="next" value="reward" id="use_reward" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="next" value="reward" id="use_reward" />
                <?php } ?></td>
              <td><label for="use_reward"><?php echo $text_use_reward; ?></label></td>
            </tr>
            <?php } ?>
            <?php if ($shipping_status) { ?>
            <tr class="highlight">
              <td><?php if ($next == 'shipping') { ?>
                <input type="radio" name="next" value="shipping" id="shipping_estimate" checked="checked" />
                <?php } else { ?>
                <input type="radio" name="next" value="shipping" id="shipping_estimate" />
                <?php } ?></td>
              <td><label for="shipping_estimate"><?php echo $text_shipping_estimate; ?></label></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <div class="cart-module">
          <div id="coupon" class="content" style="display: <?php echo ($next == 'coupon' ? 'block' : 'none'); ?>;">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="box_input">
              <i class="title"><?php echo $entry_coupon; ?></i>
              <div><input type="text" name="coupon" value="<?php echo $coupon; ?>" /></div>
              <input type="hidden" name="next" value="coupon" />
            </div>
            <div class="box_actions"><input type="submit" class="mbut morange mshadow" value="<?php echo $button_coupon; ?>" /></div>
          </form>
          </div>
          <div id="voucher" class="content" style="display: <?php echo ($next == 'voucher' ? 'block' : 'none'); ?>;">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="box_input">
              <i class="title"><?php echo $entry_voucher; ?></i>
              <div><input type="text" name="voucher" value="<?php echo $voucher; ?>" /></div>
              <input type="hidden" name="next" value="voucher" />
            </div>
            <div class="box_actions"><input type="submit" class="mbut morange mshadow" value="<?php echo $button_voucher; ?>" /></div>
          </form>
          </div>
          <div id="reward" class="content" style="display: <?php echo ($next == 'reward' ? 'block' : 'none'); ?>;">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="box_input">
              <i class="title"><?php echo $entry_reward; ?></i>
              <div><input type="text" name="reward" value="<?php echo $reward; ?>" /></div>
              <input type="hidden" name="next" value="reward" />
            </div>
            <div class="box_actions"><input type="submit" class="mbut morange mshadow" value="<?php echo $button_reward; ?>" /></div>
          </form>
          </div>
          <div id="shipping" class="content" style="display: <?php echo ($next == 'shipping' ? 'block' : 'none'); ?>;">
            <p><?php echo $text_shipping_detail; ?></p>
            <div class="box_input">
              <i class="title"><?php echo $entry_country; ?> <span class="required">*</span></i>
              <div>
                <select name="country_id">
                  <option value=""><?php echo $text_select; ?></option>
                  <?php foreach ($countries as $country) { ?>
                  <?php if ($country['country_id'] == $country_id) { ?>
                  <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="box_input">
              <i class="title"><?php echo $entry_zone; ?> <span class="required">*</span></i>
              <div>
                <select name="zone_id">
                </select>
              </div>
            </div>
            <div class="box_input">
              <i class="title"><?php echo $entry_postcode; ?> <span id="postcode-required" class="required">*</span></i>
              <div><input type="text" name="postcode" value="<?php echo $postcode; ?>" /></div>
            </div>
            <div class="box_actions"><input type="button" id="button-quote" class="mbut morange mshadow" value="<?php echo $button_quote; ?>" /></div>
          </div>
        </div>
        <?php } ?>
        <ul class="mini-cart-total">
          <?php foreach ($totals as $total) { ?>
          <li><?php echo $total['title']; ?>:<span><?php echo $total['text']; ?></span></li>
          <?php } ?>
        </ul>
        <a class="mbut mgreen mshadow m_icon_checkout" href="<?php echo $checkout; ?>"><b><?php echo $button_checkout; ?></b></a>
      </div>
      <!-- END CART RIGHT -->
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
$('input[name=\'next\']').bind('change', function() {
	$('.cart-module > div').hide();
	
	$('#' + this.value).show();
});
//--></script>
<?php if ($shipping_status) { ?>
<script type="text/javascript"><!--
$('#button-quote').live('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/quote',
		type: 'post',
		data: 'country_id=' + $('select[name=\'country_id\']').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
		dataType: 'json',		
		beforeSend: function() {
			$('#button-quote').attr('disabled', true);
			$('#button-quote').after('<span class="wait">&nbsp;</span>');
		},
		complete: function() {
			$('#button-quote').attr('disabled', false);
			$('.wait').remove();
		},		
		success: function(json) {
			$('.success, .warning, .attention, .error').remove();			
						
			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
					$('.warning').fadeIn('slow');
					
					$('html, body').animate({ scrollTop: 0 }, 'slow'); 
				}	
							
				if (json['error']['country']) {
					$('select[name=\'country_id\']').after('<span class="error error_top">' + json['error']['country'] + '</span>');
				}	
				
				if (json['error']['zone']) {
					$('select[name=\'zone_id\']').after('<span class="error error_top">' + json['error']['zone'] + '</span>');
				}
				
				if (json['error']['postcode']) {
					$('input[name=\'postcode\']').after('<span class="error error_top">' + json['error']['postcode'] + '</span>');
				}					
			}
			
			if (json['shipping_method']) {
				html  = '<h2><?php echo $text_shipping_method; ?></h2>';
				html += '<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">';
				html += '  <table class="radio">';
				
				for (i in json['shipping_method']) {
					html += '<tr>';
					html += '  <td colspan="3"><b>' + json['shipping_method'][i]['title'] + '</b></td>';
					html += '</tr>';
				
					if (!json['shipping_method'][i]['error']) {
						for (j in json['shipping_method'][i]['quote']) {
							html += '<tr class="highlight">';
							
							if (json['shipping_method'][i]['quote'][j]['code'] == '<?php echo $shipping_method; ?>') {
								html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" checked="checked" /></td>';
							} else {
								html += '<td><input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" id="' + json['shipping_method'][i]['quote'][j]['code'] + '" /></td>';
							}
								
							html += '  <td><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['title'] + '</label></td>';
							html += '  <td style="text-align: right;"><label for="' + json['shipping_method'][i]['quote'][j]['code'] + '">' + json['shipping_method'][i]['quote'][j]['text'] + '</label></td>';
							html += '</tr>';
						}		
					} else {
						html += '<tr>';
						html += '  <td colspan="3"><div class="error">' + json['shipping_method'][i]['error'] + '</div></td>';
						html += '</tr>';						
					}
				}
				
				html += '  </table>';
				html += '  <br />';
				html += '  <input type="hidden" name="next" value="shipping" />';
				
				<?php if ($shipping_method) { ?>
				html += '  <input type="submit" value="<?php echo $button_shipping; ?>" id="button-shipping" class="button" />';	
				<?php } else { ?>
				html += '  <input type="submit" value="<?php echo $button_shipping; ?>" id="button-shipping" class="button" disabled="disabled" />';	
				<?php } ?>
							
				html += '</form>';
				
				$.colorbox({
					overlayClose: true,
					opacity: 0.5,
					width: '600px',
					height: '400px',
					href: false,
					html: html
				});
				
				$('input[name=\'shipping_method\']').bind('change', function() {
					$('#button-shipping').attr('disabled', false);
				});
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;</span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<?php } ?>
<?php echo $footer; ?>