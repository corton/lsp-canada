<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_AFFILIATE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_affiliate"><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_your_payment; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_tax; ?></i>
      <div><input type="text" name="tax" value="<?php echo $tax; ?>" /></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_payment; ?></i>
      <div>
        <?php if ($payment == 'cheque') { ?>
        <input type="radio" name="payment" value="cheque" id="cheque" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="payment" value="cheque" id="cheque" />
        <?php } ?>
        <label for="cheque"><?php echo $text_cheque; ?></label>
        <?php if ($payment == 'paypal') { ?>
        <input type="radio" name="payment" value="paypal" id="paypal" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="payment" value="paypal" id="paypal" />
        <?php } ?>
        <label for="paypal"><?php echo $text_paypal; ?></label>
        <?php if ($payment == 'bank') { ?>
        <input type="radio" name="payment" value="bank" id="bank" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="payment" value="bank" id="bank" />
        <?php } ?>
        <label for="bank"><?php echo $text_bank; ?></label>
      </div>
    </div>
    <div class="payment" id="payment-cheque">
        <p class="ptitle"><?php echo $text_cheque; ?></p>
        <div class="box_input" id="payment-cheque">
          <i class="title"><?php echo $entry_cheque; ?></i>
          <div><input type="text" name="cheque" value="<?php echo $cheque; ?>" /></div>
        </div>
    </div>
    <div class="payment" id="payment-paypal">
        <p class="ptitle"><?php echo $text_paypal; ?></p>
        <div class="box_input">
          <i class="title"><?php echo $entry_paypal; ?></i>
          <div><input type="text" name="paypal" value="<?php echo $paypal; ?>" /></div>
        </div>
    </div>
    <div class="payment" id="payment-bank">
        <p class="ptitle"><?php echo $text_bank; ?></p>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_name; ?></i>
          <div><input type="text" name="bank_name" value="<?php echo $bank_name; ?>" /></div>
        </div>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_branch_number; ?></i>
          <div><input type="text" name="bank_branch_number" value="<?php echo $bank_branch_number; ?>" /></div>
        </div>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_swift_code; ?></i>
          <div><input type="text" name="bank_swift_code" value="<?php echo $bank_swift_code; ?>" /></div>
        </div>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_account_name; ?></i>
          <div><input type="text" name="bank_account_name" value="<?php echo $bank_account_name; ?>" /></div>
        </div>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_account_number; ?></i>
          <div><input type="text" name="bank_account_number" value="<?php echo $bank_account_number; ?>" /></div>
        </div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
      <a href="<?php echo $back; ?>" class="mbut mgray mshadow m_icon_back"><b><?php echo $button_back; ?></b></a>
    </div>
  </div>
  <!-- END -->
  </form>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
$('input[name=\'payment\']').bind('change', function() {
	$('.payment').hide();
	
	$('#payment-' + this.value).show();
});

$('input[name=\'payment\']:checked').trigger('change');
//--></script> 
<?php echo $footer; ?>