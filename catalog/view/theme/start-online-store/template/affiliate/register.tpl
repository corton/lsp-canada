<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_AFFILIATE">
<div id="notification"></div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_affiliate"><?php echo $heading_title; ?></h1>
  <p class="pinfo_account"><?php echo $text_account_already; ?><br /><?php echo $text_signup; ?></p>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_your_details; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_firstname; ?> <span class="required">*</span></i>
      <div><input type="text" name="firstname" value="<?php echo $firstname; ?>" /><?php if ($error_firstname) { ?><span class="error"><?php echo $error_firstname; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_lastname; ?> <span class="required">*</span></i>
      <div><input type="text" name="lastname" value="<?php echo $lastname; ?>" /><?php if ($error_lastname) { ?><span class="error"><?php echo $error_lastname; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_email; ?> <span class="required">*</span></i>
      <div><input type="text" name="email" value="<?php echo $email; ?>" /><?php if ($error_email) { ?><span class="error"><?php echo $error_email; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_telephone; ?> <span class="required">*</span></i>
      <div><input type="text" name="telephone" value="<?php echo $telephone; ?>" /><?php if ($error_telephone) { ?><span class="error"><?php echo $error_telephone; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_fax; ?></i>
      <div><input type="text" name="fax" value="<?php echo $fax; ?>" /></div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_your_address; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_company; ?></i>
      <div><input type="text" name="company" value="<?php echo $company; ?>" /></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_website; ?></i>
      <div><input type="text" name="website" value="<?php echo $website; ?>" /></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_address_1; ?> <span class="required">*</span></i>
      <div><input type="text" name="address_1" value="<?php echo $address_1; ?>" /><?php if ($error_address_1) { ?><span class="error"><?php echo $error_address_1; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_address_2; ?></i>
      <div><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_city; ?> <span class="required">*</span></i>
      <div><input type="text" name="city" value="<?php echo $city; ?>" /><?php if ($error_city) { ?><span class="error"><?php echo $error_city; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_postcode; ?> <span id="postcode-required" class="required">*</span></i>
      <div><input type="text" name="postcode" value="<?php echo $postcode; ?>" /><?php if ($error_postcode) { ?><span class="error"><?php echo $error_postcode; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_country; ?> <span class="required">*</span></i>
      <div>
        <select name="country_id">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($countries as $country) { ?>
            <?php if ($country['country_id'] == $country_id) { ?>
            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select>
        <?php if ($error_country) { ?><span class="error"><?php echo $error_country; ?></span><?php } ?>
      </div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_zone; ?> <span class="required">*</span></i>
      <div>
        <select name="zone_id">
        </select>
        <?php if ($error_zone) { ?><span class="error"><?php echo $error_zone; ?></span><?php } ?>
      </div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_payment; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_tax; ?></i>
      <div><input type="text" name="tax" value="<?php echo $tax; ?>" /></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_payment; ?></i>
      <div>
        <?php if ($payment == 'cheque') { ?>
        <input type="radio" name="payment" value="cheque" id="cheque" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="payment" value="cheque" id="cheque" />
        <?php } ?>
        <label for="cheque"><?php echo $text_cheque; ?></label>
        <?php if ($payment == 'paypal') { ?>
        <input type="radio" name="payment" value="paypal" id="paypal" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="payment" value="paypal" id="paypal" />
        <?php } ?>
        <label for="paypal"><?php echo $text_paypal; ?></label>
        <?php if ($payment == 'bank') { ?>
        <input type="radio" name="payment" value="bank" id="bank" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="payment" value="bank" id="bank" />
        <?php } ?>
        <label for="bank"><?php echo $text_bank; ?></label>
      </div>
    </div>
    <div class="payment" id="payment-cheque">
        <p class="ptitle"><?php echo $text_cheque; ?></p>
        <div class="box_input" id="payment-cheque">
          <i class="title"><?php echo $entry_cheque; ?></i>
          <div><input type="text" name="cheque" value="<?php echo $cheque; ?>" /></div>
        </div>
    </div>
    <div class="payment" id="payment-paypal">
        <p class="ptitle"><?php echo $text_paypal; ?></p>
        <div class="box_input">
          <i class="title"><?php echo $entry_paypal; ?></i>
          <div><input type="text" name="paypal" value="<?php echo $paypal; ?>" /></div>
        </div>
    </div>
    <div class="payment" id="payment-bank">
        <p class="ptitle"><?php echo $text_bank; ?></p>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_name; ?></i>
          <div><input type="text" name="bank_name" value="<?php echo $bank_name; ?>" /></div>
        </div>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_branch_number; ?></i>
          <div><input type="text" name="bank_branch_number" value="<?php echo $bank_branch_number; ?>" /></div>
        </div>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_swift_code; ?></i>
          <div><input type="text" name="bank_swift_code" value="<?php echo $bank_swift_code; ?>" /></div>
        </div>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_account_name; ?></i>
          <div><input type="text" name="bank_account_name" value="<?php echo $bank_account_name; ?>" /></div>
        </div>
        <div class="box_input">
          <i class="title"><?php echo $entry_bank_account_number; ?></i>
          <div><input type="text" name="bank_account_number" value="<?php echo $bank_account_number; ?>" /></div>
        </div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_your_password; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_password; ?> <span class="required">*</span></i>
      <div><input type="password" name="password" value="<?php echo $password; ?>" /><?php if ($error_password) { ?><span class="error"><?php echo $error_password; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_confirm; ?> <span class="required">*</span></i>
      <div><input type="password" name="confirm" value="<?php echo $confirm; ?>" /><?php if ($error_confirm) { ?><span class="error"><?php echo $error_confirm; ?></span><?php } ?></div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <?php if ($text_agree) { ?>
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" /> <?php if ($agree) { ?>
      <input type="checkbox" name="agree" value="1" checked="checked" /><?php } else { ?>
      <input type="checkbox" name="agree" value="1" /><?php } ?>
      <?php echo $text_agree; ?>
    </div>
    <?php } else { ?>
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
    </div>
    <?php } ?>
  </div>
  <!-- END -->
  </form>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=affiliate/register/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;</span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('input[name=\'payment\']').bind('change', function() {
	$('.payment').hide();
	
	$('#payment-' + this.value).show();
});

$('input[name=\'payment\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	width: 640,
	height: 480
});
//--></script> 
<?php echo $footer; ?>