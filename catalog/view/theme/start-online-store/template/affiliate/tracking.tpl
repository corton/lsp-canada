<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_AFFILIATE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_affiliate"><?php echo $heading_title; ?></h1>
  <p class="pinfo_account"><?php echo $text_description; ?></p>
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_code; ?></p>
    <div class="box_input">
      <div><textarea><?php echo $code; ?></textarea></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $text_generator; ?></i>
      <div><input type="text" name="product" value="" /></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $text_link; ?></i>
      <div><textarea name="link"></textarea></div>
    </div>
  </div>
  <!-- END -->
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="mbut mgray mshadow"><b><?php echo $button_continue; ?></b></a></div>
  </div>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=affiliate/tracking/autocomplete&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.link
					}
				}));
			}
		});
	},
	select: function(event, ui) {
		$('input[name=\'product\']').attr('value', ui.item.label);
		$('textarea[name=\'link\']').attr('value', ui.item.value);
						
		return false;
	},
	focus: function(event, ui) {
      return false;
   }
});
//--></script>
<?php echo $footer; ?>