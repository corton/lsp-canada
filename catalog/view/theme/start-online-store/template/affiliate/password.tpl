<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_AFFILIATE">
<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_affiliate"><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <!-- START -->
  <div class="box_account">
    <p class="ptitle"><?php echo $text_password; ?></p>
    <div class="box_input">
      <i class="title"><?php echo $entry_password; ?> <span class="required">*</span></i>
      <div><input type="password" name="password" value="<?php echo $password; ?>" /><?php if ($error_password) { ?><span class="error"><?php echo $error_password; ?></span><?php } ?></div>
    </div>
    <div class="box_input">
      <i class="title"><?php echo $entry_confirm; ?> <span class="required">*</span></i>
      <div><input type="password" name="confirm" value="<?php echo $confirm; ?>" /><?php if ($error_confirm) { ?><span class="error"><?php echo $error_confirm; ?></span><?php } ?></div>
    </div>
  </div>
  <!-- END -->
  <!-- START -->
  <div class="box_account">
    <div class="box_actions">
      <input type="submit" value="<?php echo $button_continue; ?>" class="mbut mgreen mshadow" />
      <a href="<?php echo $back; ?>" class="mbut mgray mshadow m_icon_back"><b><?php echo $button_back; ?></b></a>
    </div>
  </div>
  <!-- END -->
  </form>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>