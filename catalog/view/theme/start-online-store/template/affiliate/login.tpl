<?php echo $header; ?>
<!-- ..................................................................... START ALL PAGE -->
<div class="all_page MY_AFFILIATE ACCOUNT_LOGIN">
<div id="notification"></div>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<!-- START COLUMN CENTER ..................................................................... -->
<div id="content" class="column-center">
  <!-- START BREADCRUMB -->
  <div class="breadcrumb">
    <p><?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?> <strong><a href="<?php echo $breadcrumb['href']; ?>" title="<?php echo $breadcrumb['text']; ?>"><?php echo $breadcrumb['text']; ?></a></strong>
    <?php } ?></p>
  </div>
  <!-- END BREADCRUMB -->
  <?php echo $content_top; ?>
  <h1 class="icon_h my_affiliate"><?php echo $heading_title; ?></h1>
  <div class="pinfo_account"><?php echo $text_description; ?></div>
  <div class="box_account">
    <p class="ptitle"><?php echo $text_returning_affiliate; ?></p>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="left"><!-- start left -->
      <div class="box_input">
        <i class="title"><?php echo $entry_email; ?> <span class="required">*</span></i>
        <div>
          <input type="text" name="email" value="<?php echo $email; ?>" />
        </div>
      </div>
      <div class="box_input">
        <i class="title"><?php echo $entry_password; ?> <span class="required">*</span></i>
        <div>
          <input type="password" name="password" value="<?php echo $password; ?>" />
        </div>
      </div>
      <div class="box_actions">
        <input type="submit" class="mbut mgreen mshadow" value="<?php echo $button_login; ?>" />
        <?php if ($redirect) { ?><input type="hidden" name="redirect" value="<?php echo $redirect; ?>" /><?php } ?>
        <a class="link_right" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
      </div>
    </div><!-- end left -->
    </form>
    <div class="right"><!-- start right -->
      <div class="new_customer">
        <strong><?php echo $text_new_affiliate; ?></strong>
        <div class="space_text"><?php echo $text_register_account; ?></div>
        <div class="bottom">
          <div class="le"></div>
          <div class="ri"><a class="mbut mblack mshadow" href="<?php echo $register; ?>"><b><?php echo $button_continue; ?></b></a></div>
        </div>
      </div>
    </div><!-- end right -->
  </div>
  <?php echo $content_bottom; ?>
</div>
<!-- END COLUMN CENTER ..................................................................... -->
</div>
<!-- ..................................................................... END ALL PAGE -->
<?php echo $footer; ?>