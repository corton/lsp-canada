<!-- START GENERAL BOX - Information -->
<div class="box">
<div class="box-information">
  <ul class="list_info_site">
    <li><?php echo $heading_title; ?>
      <ul>
        <?php foreach ($informations as $information) { ?>
        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
        <?php } ?>
        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
        <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
      </ul>
    </li>
  </ul>
</div>
</div>
<!-- END GENERAL BOX - Information -->