<?php if (count($languages) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div class="language" id="language">
    <?php foreach ($languages as $language) { ?>
    <div class="mini_box" onclick="$('input[name=\'language_code\']').attr('value', '<?php echo $language['code']; ?>'); $(this).parent().parent().submit();">
      <span><i><?php echo $language['name']; ?></i></span>
      <img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" />
    </div>
    <?php } ?>
    <input type="hidden" name="language_code" value="" />
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  </div>
</form>
<?php } ?>