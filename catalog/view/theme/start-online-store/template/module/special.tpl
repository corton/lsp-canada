<!-- START GENERAL BOX - Specials -->
<div class="box">
<div class="box-heading"><strong class="title"><?php echo $heading_title; ?></strong></div>
<div class="box-content">
  <div class="box-sbr">
    <?php foreach ($products as $product) { ?>
    <div class="small sbr_left">
        <h3><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a></h3>
        <div class="image"><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <div class="price">
        <?php if ($product['price']) { ?>
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span>
          <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
        <?php } ?>
        </div>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <span class="rating_star star<?php echo $product['rating']; ?>">&nbsp;</span>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
  </div>
</div>
</div>
<!-- END GENERAL BOX - Specials -->