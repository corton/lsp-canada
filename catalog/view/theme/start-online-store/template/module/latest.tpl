<!-- START GENERAL BOX - Latest -->
<div class="box box_latest">
<div class="box-heading"><strong class="title"><?php echo $heading_title; ?></strong></div>
<div class="box-content">
  <div class="box-product">
    <span class="green_icon">NEW</span>
    <!-- START ID CAROUSEL -->
    <div id="carousel_latest">
      <ul class="jcarousel-skin-latest">
        <?php foreach ($products as $product) { ?>
        <li>
          <!-- START ITEM -->
          <div class="div">
            <h3><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a></h3>
            <div class="image">
              <?php if ($product['thumb']) { ?><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a><?php } ?>
            </div>
            <div class="cart">
              <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" />
              <div class="info"><a href="<?php echo $product['href']; ?>"><span><i>Info</i></span>&nbsp;</a></div>
            </div>
            <?php if ($product['rating']) { ?>
            <div class="rating">
              <i><?php echo $product['reviews']; ?></i>
              <span class="rating_star star<?php echo $product['rating']; ?>">&nbsp;</span>
            </div>
            <?php } ?>
            <?php if ($product['price']) { ?>
            <div class="price">
              <?php if (!$product['special']) { ?>
              <?php echo $product['price']; ?>
              <?php } else { ?>
              <span class="price-new"><?php echo $product['special']; ?></span>
              <span class="price-old"><?php echo $product['price']; ?></span>
              <?php } ?>
            </div>
            <?php } ?>
          </div>
          <!-- END ITEM -->
        </li>
        <?php } ?>
      </ul>
    </div>
    <!-- END ID CAROUSEL -->
  </div>
</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#carousel_latest ul').jcarousel({
        vertical: false,
        visible: 1,
        scroll: 1
    });
});
</script>
<!-- END GENERAL BOX - Latest -->
