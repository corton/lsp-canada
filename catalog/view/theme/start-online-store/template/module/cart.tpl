<!-- START CART -->
<a class="mini_shopping_cart" href="<?php echo $cart; ?>"><?php echo $heading_title; ?></a>
<div class="cart">
<div id="cart" class="">
  <div class="heading">
    <strong><?php echo $heading_title; ?></strong>
    <p><a><span id="cart-total"><?php echo $text_items; ?></span></a></p>
  </div>
  <div class="content">
    <?php if ($products || $vouchers) { ?>
    <div class="preview_cart">
      <div class="left">
        <div class="checkout">
          <a class="mbut mgreen mshadow m_icon_checkout" href="<?php echo $checkout; ?>"><b><?php echo $text_checkout; ?></b></a>
          <a class="mbut mgray mshadow m_icon_view_cart" href="<?php echo $cart; ?>"><b><?php echo $text_cart; ?></b></a>
        </div>
        <ul class="mini-cart-total">
          <?php foreach ($totals as $total) { ?>
          <li><?php echo $total['title']; ?>:<span><?php echo $total['text']; ?></span></li>
          <?php } ?>
        </ul>
      </div>
      <?php foreach ($products as $product) { ?>
      <!-- START -->
      <div class="mini-cart-info">
       <div class="image">
         <?php if ($product['thumb']) { ?>
         <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
         <?php } ?>
       </div>
       <p>
         <?php echo $product['quantity']; ?> x <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
         <?php foreach ($product['option'] as $option) { ?><br />- <?php echo $option['name']; ?> <?php echo $option['value']; ?><?php } ?>
       </p>
       <div class="status">
         <b><?php echo $product['total']; ?></b>
         <div class="cart_options remove"><span><i><?php echo $button_remove; ?></i></span><a onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $product['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $product['key']; ?>' + ' #cart > *');" title="<?php echo $button_remove; ?>">&nbsp;</a></div>
       </div>
      </div>
      <!-- END -->
      <?php } ?>
      <?php foreach ($vouchers as $voucher) { ?>
      <!-- START -->
      <div class="mini-cart-info mini-cart-voucher">
       <div class="image"></div>
       <p class="title-voucher"><?php echo $voucher['description']; ?></p>
       <div class="status">
         <b><?php echo $voucher['amount']; ?></b>
         <div class="cart_options remove"><span><i><?php echo $button_remove; ?></i></span><a onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $voucher['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $voucher['key']; ?>' + ' #cart > *');" title="<?php echo $button_remove; ?>">&nbsp;</a></div>
       </div>
      </div>
      <!-- END -->
      <?php } ?>
     </div>
    <?php } else { ?>
    <div class="empty"><?php echo $text_empty; ?></div>
    <?php } ?>
  </div>
</div>
</div>
<!--END CART -->