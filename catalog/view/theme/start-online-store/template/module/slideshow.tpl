<div class="slideshow">
    <div class="slider-wrapper theme-default">
        <div id="slider<?php echo $module; ?>" class="nivoSlider">
            <?php foreach ($banners as $banner) { ?>
            <?php if ($banner['link']) { ?>
            <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" data-thumb="<?php echo $banner['image']; ?>" /></a>
            <?php } else { ?>
            <img src="<?php echo $banner['image']; ?>" data-thumb="<?php echo $banner['image']; ?>" />
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
$(window).load(function() {
	$('#slider<?php echo $module; ?>').nivoSlider();
});
</script>
