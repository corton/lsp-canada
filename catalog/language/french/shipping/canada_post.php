<?php
// Text
$_['text_title'] 			= 'Poste Canada';
$_['text_grams'] 			= 'grammes ou moins';
$_['text_delivery'] 		= 'Expected Delivery: ';

$_['text_pc']    	   		= 'Courrier prioritaire';
$_['text_xpd']    	   		= 'Acc&eacute;l&eacute;rer';
$_['text_reg']    	   		= 'Standard';
$_['text_xp']    	   		= 'Xpresspost';
$_['text_pwu']    	   		= 'Prioritaire &Eacute;tats-Unis';
$_['text_xpu']    	   		= 'Xpresspost &Eacute;tats-Unis';
$_['text_euc']    	   		= 'Acc&eacute;l&eacute;r&eacute; &Eacute;tats-Unis commercial';
$_['text_spa']    	   		= 'Petit colis par Avion';
$_['text_sps']    	   		= 'Petit Colis par Surface';
$_['text_pwi']    	   		= 'Prioritaire Internationale';
$_['text_xpi']    	   		= 'XPressPost Internationale';
$_['text_par']    	   		= 'Colis de surface';

//Error
$_['error_not_retrieved']   = 'Le tarif n#8217;est pas disponible pour cette adresse.';
$_['error_post_code']   	= 'Un code postal valide est requis';
$_['error_no_rates']   		= 'Aucun tarif n&#8217;a pu &ecirc;tre trouv&eacute';
?>