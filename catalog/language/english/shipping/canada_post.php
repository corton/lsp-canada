<?php
// Text
$_['text_title'] 			= 'Canada Post';
$_['text_grams'] 			= 'grams or less';
$_['text_delivery'] 		= 'Expected Delivery: ';

$_['text_1010']    	   		= 'Regular';
$_['text_1020']    	   		= 'Expedited';
$_['text_1030']    	   		= 'Xpresspost';
$_['text_1040']    	   		= 'Priority Courier';
$_['text_2000']    	   		= 'Tracked Packet - USA';
$_['text_2005']    	   		= 'Small Packets Surface';
$_['text_2015']    	   		= 'Small Packets Air';
$_['text_2020']    	   		= 'Expedited US Business';
$_['text_2030']    	  		= 'Xpresspost USA';
$_['text_2040']    	   		= 'Priority Worldwide USA';
$_['text_3000']    	   		= 'Tracked Packet - INTL';
$_['text_3005']    	   		= 'Small Packets Surface';
$_['text_3010']    	   		= 'Parcel Surface';
$_['text_3015']    	   		= 'Small Packets Air';
$_['text_3025']    	   		= 'XPressPost International';
$_['text_3040']    	   		= 'Priority Worldwide INTL';

//Error
$_['error_not_retrieved']   = 'No rates could be retrieved.';
$_['error_post_code']   	= 'Post Code Required for rates.';
$_['error_no_rates']   		= 'No rates available for this address.';
?>