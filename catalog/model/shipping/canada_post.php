<?php
//-----------------------------------------
// Author: Qphoria@gmail.com
// Web: http://www.OpenCartGuru.com/
//-----------------------------------------
class ModelShippingCanadaPost extends Model {

	public function getQuote($address) {
		$classname = str_replace('vq2-catalog_model_shipping_', '', basename(__FILE__, '.php'));
        $this->load->language('shipping/' . $classname);


	    if ($this->config->get($classname . '_status')) {

			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get($classname . '_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

			if (!$this->config->get($classname . '_geo_zone_id')) {
				$status = TRUE;
			} elseif ($query->num_rows) {
				$status = TRUE;
			} else {
				$status = FALSE;
			}
        } else {
            $status = FALSE;
        }

		$method_data = array();

		if ($status) {

			$this->load->language('shipping/' . $classname);

			// canada_post does not allow zero weight. All weights in kg. lengths in cm
			$shipping_weight = ($this->cart->getWeight() == '0') ? '0.1' : $this->cart->getWeight();
			$locale_unit = 'kg';
			$weight_unit = 'KGS';
			$locale_weight_class_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "weight_class wc LEFT JOIN " . DB_PREFIX . "weight_class_description wcd ON (wc.weight_class_id = wcd.weight_class_id) WHERE wcd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND wcd.unit = '" . $locale_unit . "'");
			$locale_id = $locale_weight_class_query->row['weight_class_id'];

			if (method_exists($this->document, 'addBreadcrumb')) { //1.4.x
				$from = $this->config->get('config_weight_class');
				$to = $locale_unit;
			} else { //v15x
				$from = $this->config->get('config_weight_class_id');
				$locale_weight_class_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "weight_class wc LEFT JOIN " . DB_PREFIX . "weight_class_description wcd ON (wc.weight_class_id = wcd.weight_class_id) WHERE wcd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND wcd.unit = '" . $locale_unit . "'");
				$to = $locale_weight_class_query->row['weight_class_id'];
			}

			if (file_exists(DIR_SYSTEM . 'library/length.php')) { // v1.4.4 and newer
				$shipping_weight = str_replace(',','',$this->weight->convert($shipping_weight, $from, $to));
			} else { // v1.4.0 and older
				$results = $this->db->query("select weight_class_id from " . DB_PREFIX . "weight_class where unit = '" . $locale_unit . "'");
				$shipping_weight = str_replace(',','',$this->weight->convert($shipping_weight, $this->config->get('config_weight_class_id'), $results->row['weight_class_id']));
			}
			$shipping_weight = round($shipping_weight,4);
			$rates = array();


			$this->load->model('localisation/country');
			$country_info = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));

			$data = '<?xml version=\"1.0\" ?>';
			$data .= "<eparcel>\n";
			$data .= "	<language>" . $this->session->data['language'] . "</language>\n";
			$data .= "	<ratesAndServicesRequest>\n";
			$data .= "		<merchantCPCID>" . $this->config->get($classname . '_mid') . "</merchantCPCID>\n";
			$data .= "		<fromPostalCode>" . $this->config->get($classname . '_postcode') . "</fromPostalCode>\n";
			$data .= "		<turnAroundTime>2</turnAroundTime>\n";
			$data .= "		<itemsPrice>" . $this->cart->getSubtotal() . "</itemsPrice>\n";
			$data .= "		<lineItems>\n";

			$lineItems = $this->config->get($classname . '_individual');
			$lmwidth = 0;
			if ($lineItems) {


				foreach ($this->cart->getProducts() as $product) {
					if (!$product['shipping']) { continue; }

					$this->load->model('catalog/product');
					$product_data = $this->model_catalog_product->getProduct($product['product_id']);
					$product_weight = 0;
					// Get option weight
					if ($product['option']) {
						foreach ($product['option'] as $option) {
							$product_data['weight'] += (float)$option['weight'];
						}
					} else {
						$product_weight = $product_data['weight'];
					}

					$product_weight *= $product['quantity'];

					$product_weight = ($product_weight == '0') ? '0.1' : $product_weight;

					// Convert Cart Weight
					if (method_exists($this->document, 'addBreadcrumb')) { //1.4.x
						$from = $product['weight_class'];
						$to = $this->config->get('ups_weight_class');
					} else { //v15x
						$from = $product['weight_class_id'];
						$locale_weight_class_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "weight_class wc LEFT JOIN " . DB_PREFIX . "weight_class_description wcd ON (wc.weight_class_id = wcd.weight_class_id) WHERE wcd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND wcd.unit = '" . $locale_unit . "'");
						$to = $locale_weight_class_query->row['weight_class_id'];
					}

					if (file_exists(DIR_SYSTEM . 'library/length.php')) { // v1.4.4 and newer
						$product_weight =  str_replace(',','',$this->weight->convert($product_weight, $from, $to));
					} else { // v1.4.0 and older
						$results = $this->db->query("select weight_class_id from " . DB_PREFIX . "weight_class where unit = '" . $locale_unit . "'");
						$product_weight =  str_replace(',','',$this->weight->convert($product_weight, $product_data['weight_class_id'], $results->row['weight_class_id']));
					}
					$product_weight = round($product_weight,4);

					$data .= "			<item>\n";
					$data .= "				<quantity>" . $product['quantity'] . "</quantity>\n";
					$data .= " 				<weight>" . $product_weight . "</weight>\n";
					$data .= "				<length>" . ((float)$product_data['length'] ? $product_data['length'] : 1) . "</length>\n";
					$data .= "				<width>" . ((float)$product_data['width'] ? $product_data['width'] : 1) . "</width>\n";
					$data .= "				<height>" . ((float)$product_data['height'] ? $product_data['height'] : 1) . "</height>\n";
					$data .= "				<description>" . str_replace("�", "", $product_data['name']) . "</description>\n";
					if ($this->config->get($classname . '_rts')) {
						$data .= "				<readyToShip/>\n";
					}
					$data .= "			</item>\n";
					(float)$lmwidth += ((float)$product_data['width'] ? (float)$product_data['width'] : 1);
				}
			} else { //group all as single item
				// Combining cart as single product.
				$data .= "			<item>\n";
				$data .= "				<quantity>1</quantity>\n";
				$data .= " 				<weight>" . $shipping_weight . "</weight>\n";
				$data .= "				<length>" . ((float)$this->config->get($classname . '_length') ? $this->config->get($classname . '_length') : 5) . "</length>\n";
				$data .= "				<width>" . ((float)$this->config->get($classname . '_width') ? $this->config->get($classname . '_width') : 5) . "</width>\n";
				$data .= "				<height>" . ((float)$this->config->get($classname . '_height') ? $this->config->get($classname . '_height') : 5) . "</height>\n";
				$data .= "				<description>Store " . $this->config->get('config_name') . "</description>\n";
				$data .= "			</item>\n";
				$lmwidth = ((float)$this->config->get($classname . '_width') ? (float)$this->config->get($classname . '_width') : 5);

			}

			$data .= "		</lineItems>\n";
			$data .= "		<city>" . $address['city'] . "</city>\n";
			$data .= "		<provOrState>" . $address['zone_code'] . "</provOrState>\n";
			$data .= "		<country>" . $address['iso_code_2'] . "</country>\n";
			$data .= "		<postalCode>" . $address['postcode'] . "</postalCode>\n";
			$data .= "	</ratesAndServicesRequest>\n";
			$data .= "</eparcel>\n";

			//printf("\n\n<!--\n%s\n-->\n\n",$data); //debug xml

			if (extension_loaded('curl')) {
				set_time_limit(25);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, "sellonline.canadapost.ca:30000");  //For testing port 30000 on the server
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2678400);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = "";
				$response = curl_exec($ch);

				// Check if any error occured
				$curl_err = '';
				if(curl_errno($ch)) {
					//echo('Curl error: ' . curl_error($ch));
					return $this->retError('Curl error: ' . curl_error($ch));
				}
				curl_close ($ch);

				if ($this->config->get($classname . '_debug')) {
					if (file_exists(DIR_LOGS . $classname . '_debug.txt')) { unlink(DIR_LOGS . $classname . '_debug.txt'); }
					$logx = New Log($classname . '_debug.txt');
					$logx->write("CANADAPOST CURL DATA SENT: " . urldecode($data));
					$logx->write("CANADAPOST CURL DATA RECV: " . $response);
				}

			} else { //fsock
				$urlInfo = parse_url('http://sellonline.canadapost.ca');
				$port = '30000';
				$scheme = '';
				$method = 'GET'; // or POST
				//$method = 'POST'; // or GET
				$path = (isset($urlInfo['path'])) ? $urlInfo['path'] : '';
				if ($method == 'GET') {
					$path .= '?' . $data;
				}
				$header  = "$method $path HTTP/1.1" . "\r\n";
				$header .= 'Host:' . $urlInfo['host'] . "\r\n";
				$header .= 'Content-Type: text/xml' . "\r\n";
				$header .= 'Connection: close' . "\r\n";
				$header .= 'Content-Length: ' . strlen($data) . "\r\n\r\n";
				$fp = @fsockopen($scheme . $urlInfo['host'], $port, $errno, $errstr, 5);
				if ($fp) {
					$response = '';
					fputs($fp, $header . $data);
					while (!feof($fp)) {
						$response .= fgets($fp, 500);
					}
					fclose($fp);

					if ($this->config->get($classname . '_debug')) {
						$logx->write("CANADAPOST FSOCK DATA SENT: " . urldecode($data));
						$logx->write("CANADAPOST FSOCK DATA RECV: " . $response);
					}
				} else {
					if ($this->config->get($classname . '_debug')) {
						$logx->write("CANADAPOST FSOCK ERROR: $errno :: $errstr");
					}
					return $this->retError($errno .'::'. $errstr);
				}
			}

			$rates = $this->parserResult($response);


			// Use Canada Letter mail if less than 500g (0.5kg), otherwise get the premium rates from the canadapost server.
			//if ((float)$lmwidth < 2 && $shipping_weight < .5 && $this->config->get($classname . '_lettermail') && $address['iso_code_2'] == 'CA') {
			if ($shipping_weight < .5 && $this->config->get($classname . '_lettermail') && $address['iso_code_2'] == 'CA') {

				//Lettermail rates if destination country is Canada
				$weight = $shipping_weight * 1000;
				$tmprates = explode(',', $this->config->get($classname . '_lettermail'));

				foreach ($tmprates as $tmprate) {
					$data = explode(':', $tmprate);

					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$rates[] = array(
								'name' 				=> $data[0] . " " . $this->language->get('text_grams'),
								'rate' 				=> $data[1],
								'shippingDate' 		=> '',
								'deliveryDate' 		=> '',
								'deliveryDayOfWeek' => '',
								'nextDayAM' 		=> '',
								'packingID' 		=> '',
							);
						}
						break;
					}
				}
			//} elseif ((float)$lmwidth < 2 && $shipping_weight < .5 && $this->config->get($classname . '_lettermail') && $address['iso_code_2'] == 'US') {
			} elseif ($shipping_weight < .5 && $this->config->get($classname . '_lettermail_us') && $address['iso_code_2'] == 'US') {

				//Lettermail rates if destination country is US
				$weight = $shipping_weight * 1000;
				$tmprates = explode(',', $this->config->get($classname . '_lettermail_us'));

				foreach ($tmprates as $tmprate) {
					$data = explode(':', $tmprate);

					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$rates[] = array(
								'name' 				=> $data[0] . " " . $this->language->get('text_grams'),
								'rate' 				=> $data[1],
								'shippingDate' 		=> '',
								'deliveryDate' 		=> '',
								'deliveryDayOfWeek' => '',
								'nextDayAM' 		=> '',
								'packingID' 		=> '',
							);
						}
						break;
					}
				}
			//} elseif ((float)$lmwidth < 2 && $shipping_weight < .5 && $this->config->get($classname . '_lettermail') && ($address['iso_code_2'] != 'CA' && $address['iso_code_2'] != 'US')) {
			} elseif ($shipping_weight < .5 && $this->config->get($classname . '_lettermail_int') && ($address['iso_code_2'] != 'CA' && $address['iso_code_2'] != 'US')) {

				//Lettermail rates if destination country is Non-US International
				$weight = $shipping_weight * 1000;
				$tmprates = explode(',', $this->config->get($classname . '_lettermail_int'));

				foreach ($tmprates as $tmprate) {
					$data = explode(':', $tmprate);

					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$rates[] = array(
								'name' 				=> $data[0] . " " . $this->language->get('text_grams'),
								'rate' 				=> $data[1],
								'shippingDate' 		=> '',
								'deliveryDate' 		=> '',
								'deliveryDayOfWeek' => '',
								'nextDayAM' 		=> '',
								'packingID' 		=> '',
							);
						}
						break;
					}
				}
			}

			if (empty($rates)) { $this->retError($this->language->get('error_no_rates')); }

			// Returns error message
			if (!is_array($rates) && $rates != null) {
				return $this->retError($rates);
			}

			// No rates returned
			if (!count($rates)) {
	    		return $this->retError($this->language->get('error_not_retrieved'));
		    }

	        $quote_data = array();
	        $canada_post        = array();
	        $i = 0;

	        // Build Comment Message for Packaging info
			if (isset($rates['packing'])) {
				$msg = "\r\n\r\nRecommended Packaging:\r\n";
				foreach ($rates['packing'] as $b => $box) {
					$msg .= "Box ". ($b+1) . ": (".$box['size'].")\r\n";
					foreach ($box['items'] as $item) {
						$msg .= "  -- " . $item['name'] . " x" . $item['qty'] . "\r\n";
					}
				}
				$this->session->data['cp_packing_msg'] = $msg;
	        	unset($rates['packing']);
			}
			//

			foreach ($rates as $rate) {

				// Extra Cost
				$rate['rate'] = $rate['rate'] + $this->config->get($classname . '_extra_cost');

				$rate_title = $rate['name'];
				if ($this->config->get($classname . '_display_date')) {
					$strDate = date('m,d,Y', strtotime($rate['deliveryDate']));
					if ($strDate != "01,01,1970" && $strDate != "12,31,1969") {
						$exDate = explode(",", $strDate);
						if (checkdate($exDate[0], $exDate[1], $exDate[2])) {
							$rate_title .= ' (' . $this->language->get('text_delivery') . date($this->language->get('date_format_short'), strtotime($rate['deliveryDate'])) . ')';
						}
					}
				}

				$quote_data[$classname . '_'.$i] = array(
					'id'    => $classname . '.' . $classname . '_'. $i, //v14x
					'code'  => $classname . '.' . $classname . '_'. $i, //v15x
					'title' => $rate_title,
					'cost'  => $rate['rate'],
					'tax_class_id' => $this->config->get($classname . '_tax_class_id'),
					'text'  => $this->currency->format($this->tax->calculate($rate['rate'], $this->config->get($classname . '_tax_class_id'), $this->config->get('config_tax')))
				);
				$i++;
			}


			$title = $this->language->get('text_title');
			if ($this->config->get($classname . '_display_weight')) {
				$title .= ' (' . $shipping_weight . $locale_unit . ')';
			}


			$method_data = array(
		        'id'           => $classname, //v14x
				'code'         => $classname, //15x
		        'title'        => $title,
		        'quote'        => $quote_data,
		        'sort_order'   => $this->config->get($classname . '_sort_order'),
		        'tax_class_id' => $this->config->get($classname . '_tax_class_id'),
		        'error'        => false
		    );
		}

		return $method_data;
	}

	function parserResult($result) {
		$classname = str_replace('vq2-catalog_model_shipping_', '', basename(__FILE__, '.php'));
		$allowedSvcs = explode(",", $this->config->get($classname . '_service'));

		// Parse Response
		$dom = new DOMDocument('1.0', 'UTF-8');
		$dom->loadXml($result);

		$eparcel = $dom->getElementsByTagName('eparcel')->item(0);

		if ($eparcel->getElementsByTagName('error')->item(0)) {
			$error = $eparcel->getElementsByTagName('error')->item(0)->nodeValue;
			return $error;
		} else {
			$ratesAndServicesResponse = $eparcel->getElementsByTagName('ratesAndServicesResponse')->item(0);
			$statusCode = $ratesAndServicesResponse->getElementsByTagName('statusCode')->item(0)->nodeValue;

			if ($statusCode == '1') {
				$products = $ratesAndServicesResponse->getElementsByTagName('product');

				$svcArray = array();
				foreach ($products as $product) {
					$id = $product->getAttribute('id');

					if (!in_array($id, $allowedSvcs)) { continue; }

					// Custom Title Check
					if ($this->language->get('text_' . $id) != "text_$id") {
						$name = $this->language->get('text_' . $id);
					} else {
						$name = $product->getElementsByTagName('name')->item(0)->nodeValue;
						//$name .= (strstr($deliveryDate, 'www.canadapost.ca') ? '' : ' (' . $deliveryDate . ')');
					}

					// Build Array
					$svcArray[$id] = array(
						'name' => $name,
						'rate' => $product->getElementsByTagName('rate')->item(0)->nodeValue,
						'shippingDate' => $product->getElementsByTagName('shippingDate')->item(0)->nodeValue,
						'deliveryDate' => $product->getElementsByTagName('deliveryDate')->item(0)->nodeValue,
						'deliveryDayOfWeek' => $product->getElementsByTagName('deliveryDayOfWeek')->item(0)->nodeValue,
						'nextDayAM' => $product->getElementsByTagName('nextDayAM')->item(0)->nodeValue,
						'packingID' => $product->getElementsByTagName('packingID')->item(0)->nodeValue,
					);
				}

				// Add Packing info to comments if available
				if (!method_exists($this->document, 'addBreadcrumb')) { // v15x only!
					if (is_object($ratesAndServicesResponse->getElementsByTagName('packing'))) {
						$packings = $ratesAndServicesResponse->getElementsByTagName('packing');
						//$packingIDs = $packing->getElementsByTagName('packingID');
						foreach ($packings as $packing) {
							$boxes = $packing->getElementsByTagName('box');
							$k=0;
							foreach ($boxes as $box) {
								$size = $box->getElementsByTagName('name')->item(0)->nodeValue;
								$svcArray['packing'][$k]['size'] = $size;
								$packedItems = $box->getElementsByTagName('packedItem');
								$j=0;
								foreach ($packedItems as $packedItem) {
									$svcArray['packing'][$k]['items'][$j] = array(
										'qty'  => $packedItem->getElementsByTagName('quantity')->item(0)->nodeValue,
										'name' => $packedItem->getElementsByTagName('description')->item(0)->nodeValue,
									);
									$j++;
								}
								$k++;
							}
						}
					}
				}//

				return $svcArray;
			} else {

			}
		}
    }


	function retError($error="unknown error") {
		$classname = str_replace('vq2-catalog_model_shipping_', '', basename(__FILE__, '.php'));
		if (strpos($error, 'Destination Postal Code/State Name/ Country is illegal.') !== false && $this->language->get('error_post_code')) {
			$error=$this->language->get('error_post_code');
		}

    	$quote_data = array();

      	$quote_data['' . $classname] = array(
        	'id'           => $classname . '.' . $classname, //v14x
			'code'         => $classname . '.' . $classname, //v15x
        	'title'        => $this->language->get('text_title'),
			'cost'         => NULL,
         	'tax_class_id' => NULL,
			'text'         => 'ERROR: ' . $error
      	);

    	$method_data = array(
		  'id'           => $classname, //v14x
		  'code'         => $classname, //v15x
		  'title'        => $this->language->get('text_title'),
		  'quote'        => $quote_data,
		  'sort_order'   => $this->config->get($classname . '_sort_order'),
		  'tax_class_id' => $this->config->get($classname . '_tax_class_id'),
		  'error'        => $error
		);
		return $method_data;
	}
}
?>